/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#ifndef _config_h_
#define _config_h_


#include <Timezone.h> // https://github.com/JChristensen/Timezone


/*
 * Define your timezone here
 */

//                       abbrev, week-of, day, month, hour, offset(min)
//                               month                      from UTC
TimeChangeRule dstRule = {"EDT", Second,  Sun, Mar,   2,    -240}; // -4 hr
TimeChangeRule stdRule = {"EST", First,   Sun, Nov,   2,    -300}; // -5 hr
Timezone tz(dstRule, stdRule);


/*
 * Define various PIN locations
 */

// Hardware Serial RX pin
#define PIN_RXDATA_INTR 0

// RTC's INT
#define PIN_RTC_INTR  2

// Capacitive touch button to silence alarm
#define PIN_TOUCH 4

// Switch used to set alarm on/off
#define PIN_ON_OFF 6

// Control for femtobuck
// If this pin is changed, also change light_init()
// and avoid pins 5 and 6 if you don't want all sorts
// of things to break (millis(), delay())
#define PIN_LIGHT 10

// Piezo buzzer
#define PIN_BUZZER 9

// Front panel buttons
#define PIN_MODE 8
#define PIN_DOWN 11
#define PIN_UP   12


// Uncomment to enable additional modes for setting display
// brightness.  Note: setting is not saved between resets.
// Uses about 384 bytes or progmem and 12 bytes of global memory
//#define ENABLE_BRIGHT_SET

#endif // _config_h_
