/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#include <EEPROM.h>

#include "button.h"

// Where in EEPROM to save the alarm_magic struct
#define EADDR_MAGIC 10
// Where in EEPROM the struct alarm_time will be
#define EADDR_ALARM (EADDR_MAGIC + offsetof(struct alarm_magic, alarm_time))

// Values for fields magic1 and magic2 in struct alarm_magic
#define EEPROM_MAGIC1 0x414c524dL // A L R M
#define EEPROM_MAGIC2 0x434c434bL // C L C K


struct button btn_touch;

struct alarm_time {
    byte hour;
    byte minute;
};

struct alarm_magic {
    uint32_t magic1;
    struct alarm_time alarm_time; // if changed, also change EADDR_ALARM
    uint32_t magic2;
};


struct alarm_time alarm_time = {0, 0};


// Defines for state machine
#define ALARM_STATE_NORMAL 0
#define ALARM_STATE_RAMP_UP 1
#define ALARM_STATE_PLAYING 2
#define ALARM_STATE_FULL_ON 3
#define ALARM_STATE_RAMP_DOWN 4
#define ALARM_STATE_GOTO_SLEEP 5


/*
 * Alarm timeline
 *
 *[ramp-up---------][full-on------][ramp-down-------]
 *                  [alarm-on]
 *                  ^alarm set time
 *
 * alarm-on (ringing) and full-on (light) start at the same time,
 * but alarm-on must end sooner then full-on
 *
 * Times ALARM_xx_TIME defines below for various stages are in minutes
 */
// How long before alarm sounds, that the wakeup light starts
#define ALARM_RAMP_UP_TIME 30

// How long to wait in FULL_ON state before moving to ramp_down
#define ALARM_FULL_ON_TIME 15

// How long to stay in the RAMP_DOWN state
#define ALARM_RAMP_DOWN_TIME 5

// How long to stay (ramp down) in the GOTO_SLEEP state
#define ALARM_GOTO_SLEEP_TIME 2

// Maximum time alarm can sound (must be less then FULL_ON)
#define ALARM_MAX_PLAY_TIME 2


// Values for light settings (range 0-255)
#define ALARM_LIGHT_FULL_ON 255
#define ALARM_LIGHT_SLEEP_START 127


byte alarm_state = ALARM_STATE_NORMAL;


void alarm_dbg_print(void) {
    DBG("Alarm: %02d:%02d %s state=%d", alarm_time.hour, alarm_time.minute,
        alarm_is_on() ? "on" : "off", alarm_state);
}

void alarm_init(void) {
    struct alarm_magic magic;

    EEPROM.get(EADDR_MAGIC, magic);
    if (magic.magic1 != EEPROM_MAGIC1 || magic.magic2 != EEPROM_MAGIC2) {
        magic.magic1 = EEPROM_MAGIC1;
        magic.magic2 = EEPROM_MAGIC2;
        magic.alarm_time = alarm_time;
        EEPROM.put(EADDR_MAGIC, magic);
        DBG("Reset alarm due to magic");
    }

    EEPROM.get(EADDR_ALARM, alarm_time);
    alarm_dbg_print();

    pinMode(PIN_TOUCH, INPUT);
    enableInterrupt(PIN_TOUCH, alarm_touch_intr, CHANGE);

    pinMode(PIN_ON_OFF, INPUT_PULLUP);
    enableInterrupt(PIN_ON_OFF, func_nop, CHANGE);

    alarm_reconfigure();
}


/*
 * Interrupt callback
 * Set alarm_touched to true whenever the capacitive touch is touched.
 * Will get set back to false in alarm_check()
 */
int alarm_touched = false;
void alarm_touch_intr(void) {
    if (arduinoInterruptedPin == PIN_TOUCH)
        alarm_touched = true;
}

void alarm_display(void) {
    if (alarm_is_on())
        disp_alpha_show_time(alarm_time.hour, alarm_time.minute);
    else
        disp_alpha_show("OFF");
}

boolean alarm_is_on(void) {
    int val = digitalRead(PIN_ON_OFF);

    if (val == LOW)
        return true;
    return false;
}

// Set the alarm (hours and minutes) in local time
void alarm_set(int hh, int mm) {
    alarm_time.hour = hh;
    alarm_time.minute = mm;
    EEPROM.put(EADDR_ALARM, alarm_time);

    alarm_display();
    alarm_reconfigure();
}

void alarm_set(const char *hhmm) {
    int hh;
    int mm;
    int n;

    n = sscanf(hhmm, "%2u:%2u", &hh, &mm);
    if (n != 2) {
        DBG("Bad alarm string: %s", hhmm);
        return;
    }

    alarm_set(hh, mm);
    alarm_dbg_print();
}


void alarm_get(int *hh, int *mm) {
    *hh = alarm_time.hour;
    *mm = alarm_time.minute;
}


/*
 * Handle an 'alarm' serial command
 */
void alarm_command(const char *cmd) {
    if (!strcmp(cmd, "reset"))
        alarm_new_state(ALARM_STATE_NORMAL);
    else if (!strcmp(cmd, "rampup"))
        alarm_new_state(ALARM_STATE_RAMP_UP);
    else if (!strcmp(cmd, "play"))
        alarm_new_state(ALARM_STATE_PLAYING);
    else if (!strcmp(cmd, "rampdown"))
        alarm_new_state(ALARM_STATE_RAMP_DOWN);
    else if (!strcmp(cmd, ""))
        alarm_dbg_print();
    else {
        alarm_set(cmd);
    }
}


struct alarm_time time_t_to_alarm_time(time_t t) {
    struct alarm_time a;
    a.hour = hour(t);
    a.minute = minute(t);
    return a;
}


/*
 * Convert struct alarm_time to minutes from midnight (00:00)
 */
int alarm_time_to_minutes(struct alarm_time at) {
    return at.hour * 60 + at.minute;
}


/*
 * Return true is current time (now) is at or after t1 but before t2
 */
boolean alarm_time_in_range(time_t now,
                            struct alarm_time t1,
                            struct alarm_time t2) {
    // convert all times to minutes from 00:00
    int m = hour(now) * 60 + minute(now);
    int m1 = alarm_time_to_minutes(t1);
    int m2 = alarm_time_to_minutes(t2);

    if (m1 < m2) {
        if (m >= m1 && m < m2)
            return true;
    }
    else if (m1 > m2) { // the times wrap past midnight
        if (m >= m1 || m < m2)
            return true;
    }
    return false;
}


/*
 * Return t2 - t1 (in minutes)
 */
int alarm_time_delta_minutes(struct alarm_time t1, struct alarm_time t2) {
    // convert all times to minutes from 00:00
    int m_t1 = alarm_time_to_minutes(t1);
    int m_t2 = alarm_time_to_minutes(t2);
    int delta = m_t2 - m_t1;

    if (delta < 0) { // times wrap around midnight
        delta = (24 * 60) - delta;
    }

    return delta;
}

int alarm_time_delta_minutes(time_t _t1, struct alarm_time t2) {
    struct alarm_time t1 = time_t_to_alarm_time(_t1);
    return alarm_time_delta_minutes(t1, t2);
}

/*
 * Return true if the struct alarm_time is the same minute as now
 */
boolean alarm_time_equal(time_t now, struct alarm_time at) {
    int delta = alarm_time_delta_minutes(now, at);
    if (delta == 0)
        return true;
    return false;
}

/*
 * Return a struct alarm_time that is delta minutes from t1
 */
struct alarm_time alarm_time_delta(struct alarm_time t1, int delta) {
    struct alarm_time t2;
    int hour;
    int minute;

    hour = t1.hour;
    minute = t1.minute + delta;

    while (minute >= 60) {
        minute -= 60;
        hour += 1;
        if (hour == 24)
            hour = 0;
    }

    while (minute < 0) {
        minute += 60;
        hour -= 1;
        if (hour == -1)
            hour = 23;
    }

    t2.hour = hour;
    t2.minute = minute;
    return t2;
}


/*
 * Do what needs to be done upon enter a new alarm_state
 */
void alarm_new_state(byte new_state) {
    alarm_new_state(new_state, ALARM_RAMP_UP_TIME);
}

void alarm_new_state(byte new_state, int ramp_up_time) {
    if (new_state == alarm_state)
        return;

    DBG("alarm state %d -> %d", alarm_state, new_state);
    alarm_state = new_state;

    switch (new_state) {
    case ALARM_STATE_NORMAL:
        light_set(0);
        break;

    case ALARM_STATE_RAMP_UP:
        light_ramp(0, ALARM_LIGHT_FULL_ON, ramp_up_time * 60);
        break;

    case ALARM_STATE_PLAYING:
        light_set(ALARM_LIGHT_FULL_ON);
        play_alarm();
        break;

    case ALARM_STATE_FULL_ON:
        break;

    case ALARM_STATE_RAMP_DOWN:
        light_ramp(ALARM_LIGHT_FULL_ON, 0, ALARM_RAMP_DOWN_TIME * 60);
        break;

    case ALARM_STATE_GOTO_SLEEP:
        light_ramp(ALARM_LIGHT_SLEEP_START, 0, ALARM_GOTO_SLEEP_TIME * 60);
        break;
    }
}


/*
 * Adjusts alarm state to the new reality.
 * To be called whenever...
 * - alarm time is changed
 * - current time is set
 * - alarm on/off switch is flipped
 * - at startup
 */
void alarm_reconfigure(void) {
    time_t now;
    byte new_state;
    struct alarm_time time_light_up; // when ramp up starts
    struct alarm_time time_alarm_end; // When alarm must stop sounding
    int ramp_up_time;
    int on = alarm_is_on();

    now = rtc_get_local_time_t();

    ramp_up_time = alarm_time_delta_minutes(now, alarm_time);
    time_light_up = alarm_time_delta(alarm_time, - ALARM_RAMP_UP_TIME);
    time_alarm_end = alarm_time_delta(alarm_time, ALARM_MAX_PLAY_TIME);

    new_state = ALARM_STATE_NORMAL;

    if (on == false) { // Alarm switched off
        // do nothing
    }
    else if (alarm_time_in_range(now, time_light_up, alarm_time)) {
        new_state = ALARM_STATE_RAMP_UP;
    }
    else if (alarm_time_in_range(now, alarm_time, time_alarm_end)) {
        new_state = ALARM_STATE_PLAYING;
    }

    alarm_new_state(new_state, ramp_up_time);
}

/*
 * Check to see if we need to change alarm state.
 * This is called in loop() unless mode change in progress.
 */
boolean alarm_check(void) {
    time_t now;
    int playing;
    struct alarm_time time_light_up; // when ramp up starts
    struct alarm_time time_light_down; // when ramp down starts
    struct alarm_time time_light_off; // when ramp down finishes
    struct alarm_time time_alarm_end; // When alarm must stop sounding
    byte new_state;
    int touch = false;
    int on = alarm_is_on();
    static int last_on = on;

    on = alarm_is_on();
    if (on != last_on) {
        last_on = on;
        alarm_reconfigure();
        alarm_display();
        alarm_dbg_print();
    }

    if (alarm_touched) {
        alarm_touched = false; // set by interrupt handler
        touch = true;
    }

    now = rtc_get_local_time_t();

    time_light_up = alarm_time_delta(alarm_time, - ALARM_RAMP_UP_TIME);
    time_light_down = alarm_time_delta(alarm_time, ALARM_FULL_ON_TIME);
    time_light_off = alarm_time_delta(time_light_down, ALARM_RAMP_DOWN_TIME);
    time_alarm_end = alarm_time_delta(alarm_time, ALARM_MAX_PLAY_TIME);

    new_state = alarm_state;
    switch (alarm_state) {
    case ALARM_STATE_NORMAL:
        if (touch) {
            new_state = ALARM_STATE_GOTO_SLEEP;
            break;
        }

        if (on == false)
            break; // Alarm switched off, do nothing further

        if (alarm_time_equal(now, time_light_up)) {
            new_state = ALARM_STATE_RAMP_UP;
            break;
        }

        break;

    case ALARM_STATE_RAMP_UP:
        if (alarm_time_equal(now, alarm_time)) {
            new_state = ALARM_STATE_PLAYING;
            break;
        }

        if (touch) {
            DBG("alarm touch early silence");
            new_state = ALARM_STATE_FULL_ON;
        }
        break;

    case ALARM_STATE_PLAYING:
        if (touch) {
            DBG("alarm touch silence");
            new_state = ALARM_STATE_FULL_ON;
            break;
        }

        if (alarm_time_equal(now, time_alarm_end)) {
            DBG("alarm auto silence");
            new_state = ALARM_STATE_FULL_ON;
            break;
        }

        // Continue playing wake up melody
        playing = play_cont();
        if (!playing) {
            DBG("alarm finished");
            new_state = ALARM_STATE_FULL_ON;
        }

        break;

    case ALARM_STATE_FULL_ON:
        if (alarm_time_equal(now, time_light_down)) {
            new_state = ALARM_STATE_RAMP_DOWN;
        }
        break;

    case ALARM_STATE_RAMP_DOWN:
    case ALARM_STATE_GOTO_SLEEP:
        if (!light_is_on())
            new_state = ALARM_STATE_NORMAL;

        break;
    }

    alarm_new_state(new_state);

    return alarm_state;
}
