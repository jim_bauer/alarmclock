/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef _button_h_
#define _button_h_


#define BUTTON_NONE 0
#define BUTTON_PRESS 1
#define BUTTON_RELEASE 2
#define BUTTON_HELD 3


struct button {
    byte pin;
    byte last;
    boolean ignore_up;
    long up_time;
    long dn_time;
    long last_held;
};

#endif // _button_h_
