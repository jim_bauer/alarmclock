/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#ifndef _debug_h_
#define _debug_h_

// Output a debug message to serial console
// This does *not* handle printing float types as Arduino's vsnprintf()
// does not include %f support due to code size

// Comment out to disable all debug (DBG) output
#define DEBUG 1

#ifdef DEBUG
char dbg_buf[100];
void debugmsg(const __FlashStringHelper *flashfmt, ...) {
    byte len = strlen_P((const char *)flashfmt);
    char fmt[len + 1];
    va_list args;

    // Copy flashfmt from flash memory to RAM so vsnprintf() can use it
    strcpy_P(fmt, (const char *)flashfmt);

    va_start(args, flashfmt);
    vsnprintf(dbg_buf, sizeof(dbg_buf), fmt, args);
    va_end(args);

    Serial.print(dbg_buf);
}

// The F() macro stores the format string in flash memory
#define DBG(fmt, args...) debugmsg(F(fmt "\n"), ##args)

#else // DEBUG
#define DBG(fmt, args...) do {} while(0)
#endif // DEBUG

#endif // _debug_h_
