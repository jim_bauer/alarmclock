/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "debug.h"
#include "config.h"

#include <Wire.h>


#include "display.h"
#include "error.h"

#define ARRAYLEN(x) (sizeof(x) / sizeof(x[0]))
#define UNUSED __attribute__((unused))

// https://github.com/GreyGnome/EnableInterrupt
// Enabling below define before the include will set the variable
// arduinoInterruptedPin that can be checked to determine which pin's
// interrupt was last triggered.
#define EI_ARDUINO_INTERRUPTED_PIN
#include <EnableInterrupt.h>


#include <TimeLib.h> // https://github.com/PaulStoffregen/Time


#define STAY_AWAKE_TIME 10000
#define STAY_AWAKE_TIME_SERIAL 30000
unsigned long next_sleep_time;



// sizes: long=4, int=2, short=2, enum=2, byte=1, char=1, boolean=1

void dbg_print_time(time_t t) {
    DBG("%04d-%02d-%02d %02d:%02d:%02d",
        year(t), month(t), day(t),
        hour(t), minute(t), second(t));
}


void reduce_power(void) {
    // from
    // https://forum.arduino.cc/index.php?topic=164146.msg1232507#msg1232507
    // Make all pins input pins with pullup resistors to
    // minimize power consumption.  Maybe saves about 5mA.
    for (byte i=0; i<20; i++) {
        if (i == LED_BUILTIN)
            continue; // but not the LED pin
        pinMode(i, INPUT_PULLUP);
    }
}

void setup() {
    time_t t;

    // There is a double reset on upload, this delay avoids seeing
    // any message before 2nd reset happens
    delay(200);

    reduce_power();

    Serial.begin(115200);
    DBG("Starting up.  Build: %s %s", __DATE__, __TIME__);
    Wire.begin(); //Join I2C bus

    disp_init();
    disp_alpha_show("INIT");

    mode_init();
    DBG("mode done");
    rtc_init();
    DBG("rtc done");
    sleep_init();
    DBG("sleep done");
    light_init();
    DBG("light done");
    alarm_init();
    DBG("alarm done");

    next_sleep_time = millis() + STAY_AWAKE_TIME;
    delay(100);

    DBG("init done");

    // Display current time on big display
    t = rtc_get_local_time_t();
    disp_main_show_time(hour(t), minute(t));
    dbg_print_time(t);

    // Display alarm time on small display
    alarm_display();

    DBG("*** setup done ***");
}

void loop() {
    time_t t;
    boolean any_serial;
    boolean alarm;
    boolean light;

    any_serial = serial_commands();
    light = light_loop();

    // stay awake a while if there was any serial input
    if (any_serial || light)
        next_sleep_time = millis() + STAY_AWAKE_TIME_SERIAL;

    if (mode_check())
        return; // we are setting something, so skip normal stuff

    if (rtc_new_minute() || any_serial) {
        t = rtc_get_local_time_t();
        disp_main_show_time(hour(t), minute(t));
        if (!any_serial)
            dbg_print_time(t);
    }

    // Small delay so we don't get overrun with debug messages
    // This will affect melody playing and light ramp timing
    //delay(100);

    // Handle time for the alarm to sound.
    // Note that this won't happen if we happen to be in a setting mode.
    // And could also fail if alarm time is around a DST on/off transition.
    alarm = alarm_check();
    if (alarm)
        return;

    // Go to sleep if it has been long enough
    if (millis() > next_sleep_time) {
        sleep();
        next_sleep_time = millis() + STAY_AWAKE_TIME;
    }
}


// NOP function for callbacks
void func_nop(void) {
}

char command_buf[10];
char value_buf[20];
int read_token(char *buf, int len) {
    int i = 0;

    for (i=0; i < len; i++) {
        delay(10);
        buf[i] = Serial.read();
        if (buf[i] == ' ') {
            buf[i] = '\0';
            return 1;
        }

        if (buf[i] == '\n' || buf[i] == -1) {
            buf[i] = '\0';
            return 2;
        }
    }

    DBG("serial input too long");
    return -1; // truncated
}



struct commands {
    const char *cmd;
    void (*func)(const char *arg);
};



struct commands commands[] = {
    {
        .cmd = "help",
        .func = help,
    },
    {
        .cmd = "date",
        .func = rtc_date_command,
    },
    {
        .cmd = "alarm",
        .func = alarm_command,
    },
    {
        .cmd = "bright",
        .func = disp_main_set_bright,
    },
    {
        .cmd = "abright",
        .func = disp_alpha_set_bright,
    },
};


void help(UNUSED const char *x) {
    DBG("date [yyyy-mm-dd | hh:mm[:ss]]");
    DBG("alarm [rampup | rampdown | play | reset | hh:mm]");
    DBG("bright <0-15>");
    DBG("abright <0-15>");
}


// Handle any debuging command from serial device
// Return true is any serial input (even if not valid)
boolean serial_commands(void) {
    char cmd[20];
    char val[20];
    int n;
    int n2 = 0;
    unsigned c;

    if (!Serial.available())
        return false;

    cmd[0] = '\0';
    val[0] = '\0';
    n = read_token(cmd, sizeof(cmd));
    if (n == 1)
        n2 = read_token(val, sizeof(val));

    if (n == -1 || n2 == -1)
        return true;

    DBG("Got %s %s", cmd, val);

    for (c=0; c < ARRAYLEN(commands); c++) {
        if (!strcmp(cmd, commands[c].cmd)) {
            (commands[c].func)(val);
            return true;
        }
    }
    if (c == ARRAYLEN(commands)) {
        DBG("Unknown command: %s", cmd);
    }
    return true;
}
