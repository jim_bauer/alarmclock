/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "button.h"


enum modes {
    MODE_NORM = 0,
    MODE_SETALARM,
    MODE_SETTIME,
    MODE_SETDATE,
    MODE_SETYEAR,
#ifdef ENABLE_BRIGHT_SET
    MODE_SETBRIGHT, // Not saved
    MODE_SETBRIGHTA, // Not saved
#endif
};

enum modes mode = MODE_NORM;
enum modes last_mode = MODE_NORM;


// Compact way to set either:
//  h m   (hours, minutes),
//  m d y (month, day, year(for leap year))
//  y     (year),
//  b     (brightness)
struct set {
    union {
        int h;
        int d;
#ifdef ENABLE_BRIGHT_SET
        int b;
#endif
    };
    int m;
    int y;
};

struct set orig;
struct set curr;

struct button btn_mode;
struct button btn_down;
struct button btn_up;


void mode_init(void) {
    button_init(&btn_mode, PIN_MODE);
    button_init(&btn_down, PIN_DOWN);
    button_init(&btn_up, PIN_UP);

    enableInterrupt(PIN_MODE, func_nop, CHANGE);
    enableInterrupt(PIN_DOWN, func_nop, CHANGE);
    enableInterrupt(PIN_UP,   func_nop, CHANGE);
}


int days_in_month(int y, int m) {
    switch (m) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        return 31;

    case 4:
    case 6:
    case 9:
    case 11:
        return 30;
    }

    // Must be feb
    if (y % 400 == 0)
        return 29;
    if (y % 100 == 0)
        return 28;
    if (y % 4 == 0)
        return 29;
    return 28;
}


/*
 * Handle setting year
 */
void mode_set_yyyy(void) {
    int up, down;
    int add = 0;

    down = button_check(&btn_down);
    if (down == BUTTON_RELEASE || down == BUTTON_HELD)
        add = -1;

    up = button_check(&btn_up);
    if (up == BUTTON_RELEASE || up == BUTTON_HELD)
        add = 1;

    if (add) {
        curr.y += add;

        // Enforce year limits:
        // - RTC clock can't handle years before 2000
        // - time_t can't handle dates beyond 2106-02-07 06:28:15 UTC
        // - (time_t is uint32 with epoch at 1970-01-01 00:00:00 UTC)
        curr.y = max(2000, curr.y);
        curr.y = min(2105, curr.y);
    }
}


/*
 * Handle setting dates (month, day of month)
 */
void mode_set_mm_dd(void) {
    int up, down;
    int add = 0;
    int d_lim;

    down = button_check(&btn_down);
    if (down == BUTTON_RELEASE || down == BUTTON_HELD)
        add = -1;

    up = button_check(&btn_up);
    if (up == BUTTON_RELEASE || up == BUTTON_HELD)
        add = 1;

    if (add) {
        d_lim = days_in_month(curr.y, curr.m);
        curr.d += add;

        if (curr.d > d_lim) {
            curr.m++;
            curr.d -= d_lim;
        }

        if (curr.d <= 0) {
            curr.m--;
            curr.d += days_in_month(curr.y, curr.m);
        }

        if (curr.m == 0)
            curr.m = 12;
        if (curr.m > 12)
            curr.m = 1;
    }
}


/*
 * Handle setting time (hour, minute)
 * Uses 24-hour time of course
 */
void mode_set_hh_mm(void) {
    int up, down;
    int add = 0;

    down = button_check(&btn_down);
    if (down == BUTTON_RELEASE || down == BUTTON_HELD)
        add = -1;

    up = button_check(&btn_up);
    if (up == BUTTON_RELEASE || up == BUTTON_HELD)
        add = 1;

    if (add) {
        curr.m += add;
        if (curr.m >= 60) {
            curr.h = (curr.h + 1) % 24;
            curr.m -= 60;
        }

        if (curr.m < 0) {
            curr.h = curr.h - 1;
            if (curr.h < 0)
                curr.h = 23;
            curr.m += 60;
        }
    }
}


#ifdef ENABLE_BRIGHT_SET
void mode_set_bright(byte bmin, byte bmax) {
    int up, down;
    int add = 0;

    down = button_check(&btn_down);
    if (down == BUTTON_RELEASE || down == BUTTON_HELD)
        add = -1;

    up = button_check(&btn_up);
    if (up == BUTTON_RELEASE || up == BUTTON_HELD)
        add = 1;

    if (add) {
        curr.b += add;
        if (curr.b > bmax)
            curr.b = bmax;

        if (curr.b < bmin)
            curr.b = bmin;

    }
}
#endif // ENABLE_BRIGHT_SET


// Mode just changed, do anything needed when exiting a mode
void mode_changed(void) {
    switch (last_mode) {
    case MODE_NORM:
        break;

    case MODE_SETALARM:
        if (curr.h != orig.h || curr.m != orig.m) {
            alarm_set(curr.h, curr.m);
            DBG("Set alarm to %d:%d", curr.h, curr.m);
        }
        break;

    case MODE_SETTIME:
        if (curr.h != orig.h || curr.m != orig.m) {
            rtc_set_local_time(curr.h, curr.m, 0);
            DBG("Set time to %d:%d", curr.h, curr.m);
            alarm_reconfigure();
        }
        break;

    case MODE_SETDATE:
        if (curr.m != orig.m || curr.d != orig.d) {
            rtc_set_local_date(curr.y, curr.m, curr.d);
            DBG("Set date to %d-%d-%d", curr.y, curr.m, curr.d);
        }
        break;

    case MODE_SETYEAR:
        if (curr.y != orig.y) {
            // In case date is end of feb and value is too large for
            // current year, change date to march 1
            if (curr.m == 2 && curr.d > days_in_month(curr.y, curr.m)) {
                curr.m = 3;
                curr.d = 1;
            }
            rtc_set_local_date(curr.y, curr.m, curr.d);
            DBG("Set date to %d-%d-%d", curr.y, curr.m, curr.d);
        }
        break;

#ifdef ENABLE_BRIGHT_SET
    case MODE_SETBRIGHT:
    case MODE_SETBRIGHTA:
        break;
#endif
    }
}

/*
 * Advance to next mode, wrapping around to first when at end
 */
int mode_next(void) {
    last_mode = mode;
    switch (mode) {
    case MODE_NORM:
        mode = MODE_SETALARM;
        break;
    case MODE_SETALARM:
        mode = MODE_SETTIME;
        break;
    case MODE_SETTIME:
        mode = MODE_SETDATE;
        break;
    case MODE_SETDATE:
        mode = MODE_SETYEAR;
        break;
    case MODE_SETYEAR:
#ifdef ENABLE_BRIGHT_SET
        mode = MODE_SETBRIGHT;
        break;
    case MODE_SETBRIGHT:
        mode = MODE_SETBRIGHTA;
        break;
    case MODE_SETBRIGHTA:
#endif
        mode = MODE_NORM;
        break;
    }

    DBG("Mode now %d", mode);
    return mode;
}


/*
 * Handle modes and mode changes
 * return false if MODE_NORM else true
 * When true is returned, we are expecting to be called again right away
 * Called every itteration in main loop
*/
boolean mode_check(void) {
    int b;
    time_t t;

    // Check for mode butter presses and switch to new mode
    b = button_check(&btn_mode);
    if (b == BUTTON_RELEASE) {
        mode_next();
        mode_changed();

        if (mode == MODE_NORM) {
            t = rtc_get_local_time_t();
            disp_main_blink(false);
            disp_main_show_time(hour(t), minute(t));
            alarm_display();
            return false;
        }

        if (mode == MODE_SETALARM) {
            alarm_get(&curr.h, &curr.m);
            orig = curr;
            disp_main_blink(true);
            disp_main_show_time(curr.h, curr.m);
            disp_alpha_show("ALRM");
        }

        if (mode == MODE_SETTIME) {
            time_t t = rtc_get_local_time_t();
            curr.h = hour(t);
            curr.m = minute(t);
            orig = curr;
            disp_main_blink(true);
            disp_main_show_time(curr.h, curr.m);
            disp_alpha_show("TIME");
        }

        if (mode == MODE_SETDATE || mode == MODE_SETYEAR) {
            time_t t = rtc_get_local_time_t();
            curr.y = year(t);
            curr.m = month(t);
            curr.d = day(t);
            orig = curr;
            disp_main_blink(true);
            if (mode == MODE_SETDATE) {
                disp_main_show_2_nums(curr.m, curr.d);
                disp_alpha_show("DATE");
            }
            else {
                disp_main_show_1_num(curr.y);
                disp_alpha_show("YEAR");
            }
        }

#ifdef ENABLE_BRIGHT_SET
        if (mode == MODE_SETBRIGHT) {
            byte bright = disp_main_get_bright();
            curr.b = bright;
            DBG("bright = %d", bright);
            disp_main_blink(true);
            disp_main_show_1_num(bright);
            disp_alpha_show("BRT1");
        }

        if (mode == MODE_SETBRIGHTA) {
            byte bright = disp_alpha_get_bright();
            curr.b = bright;
            DBG("bright = %d", bright);
            disp_main_blink(true);
            disp_main_show_1_num(bright);
            disp_alpha_show("BRTA");
        }
#endif
    }

    // Do whatever is needed for the current mode

    if (mode == MODE_NORM)
        return false;

    if (mode == MODE_SETALARM || mode == MODE_SETTIME) {
        mode_set_hh_mm();
        disp_main_show_time(curr.h, curr.m);
    }

    if (mode == MODE_SETDATE) {
        mode_set_mm_dd();
        disp_main_show_2_nums(curr.m, curr.d);
    }

    if (mode == MODE_SETYEAR) {
        mode_set_yyyy();
        disp_main_show_1_num(curr.y);
    }

#ifdef ENABLE_BRIGHT_SET
    if (mode == MODE_SETBRIGHT) {
        mode_set_bright(DISP_MAIN_MIN_BRIGHT, DISP_MAIN_MAX_BRIGHT);
        disp_main_set_bright(curr.b);
        disp_main_show_1_num(curr.b);
    }

    if (mode == MODE_SETBRIGHTA) {
        mode_set_bright(DISP_ALPHA_MIN_BRIGHT, DISP_ALPHA_MAX_BRIGHT);
        disp_alpha_set_bright(curr.b);
        disp_main_show_1_num(curr.b);
    }
#endif

    return true;
}
