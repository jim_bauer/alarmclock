/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


// Some of this code is from or inspired by
// https://jmsarduino.blogspot.com/2009/05/click-for-press-and-hold-for-b.html


#include "button.h"

#define DEBOUNCE 20L // ms
#define HOLDTIME 1000L // ms

#define HELD_RATE1      100L  // return held every x ms
#define HELD_RATE1_TIME 5000L // for x ms
#define HELD_RATE2      10L   // then return held every x ms


void button_init(struct button *btn, int pin) {
    btn->pin = pin;
    btn->last = HIGH;
    btn->up_time = 0;
    btn->dn_time = 0;
    btn->ignore_up = false;
    btn->last_held = 0;
    pinMode(pin, INPUT_PULLUP);
}


int button_check(struct button *btn) {
    int val = digitalRead(btn->pin);
    int rc = BUTTON_NONE;
    long now = millis();

    // Check for press and store down-time
    if (val == LOW && btn->last == HIGH && now - btn->up_time) {
        rc = BUTTON_PRESS;
        btn->dn_time = now;
        btn->last_held = 0;
    }

    // Check for release and store up-time
    if (val == HIGH && btn->last == LOW && now - btn->dn_time > DEBOUNCE) {
        if (btn->ignore_up == false)
            rc = BUTTON_RELEASE;
        else
            btn->ignore_up = false;

        btn->up_time = now;
    }

    // Check for button held down
    // Throttle how often we return BUTTON_HELD (see HELD_RATE* defines above)
    if (val == LOW && (now - btn->dn_time) > HOLDTIME) {
        if (now - btn->dn_time < HELD_RATE1_TIME) {
            if (now - btn->last_held > HELD_RATE1) {
                rc = BUTTON_HELD;
            }
        }
        else {
            if (now - btn->last_held > HELD_RATE2) {
                rc = BUTTON_HELD;
            }
        }

        if (rc == BUTTON_HELD) {
            btn->ignore_up = true;
            btn->last_held = now;
        }
    }

    btn->last = val;
    if (rc != BUTTON_NONE) {
        DBG("Button %d, state %s", btn->pin,
            rc==BUTTON_HELD ? "HELD" :
            rc==BUTTON_RELEASE ? "RELEASE" :
            rc==BUTTON_PRESS ? "PRESS" : "NONE");
    }
    return rc;
}
