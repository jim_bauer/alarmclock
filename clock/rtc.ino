/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


// https://www.sparkfun.com/products/16281
// Add SparkFun RV-8803 library
#include <SparkFun_RV8803.h>


RV8803 rtc;


char days[][4] = {"Su", "Mo",  "Tu",  "We",  "Th",  "Fr",  "Sa", "?"};
#define DAYNAME(w) days[w>6 ? 7 : w]


// Calc weekday from date (from http://stackoverflow.com/a/21235587)
// 0 = Sunday, 6 = Saturday
uint16_t rtc_weekday_from_date(uint16_t y, uint16_t m, uint16_t d) {
    return (d += m<3 ? y--: y-2, 23*m/9 + d + 4 + y/4 -y/100 + y/400)%7;
}


int rtc_init(void) {
    if (rtc.begin() == false) {
        DBG("RTC init failed");
        display_error(ERR_RTC_INIT);
        return -1;
    }

    // Make sure we are in 24-hour mode
    // (why is 12-hr even an option much less the default?)
    rtc.set24Hour();

    rtc.disableAllInterrupts();
    rtc.clearAllInterruptFlags();
    rtc.setPeriodicTimeUpdateFrequency(TIME_UPDATE_1_MINUTE);
    rtc.enableHardwareInterrupt(UPDATE_INTERRUPT); // needed for periodic intr
    rtc.setAlarmWeekday(0);

    return 0;
}


time_t rtc_get_utc_time_t(void) {
    tmElements_t tm;

    rtc.updateTime();
    tm.Year = rtc.getYear() - 1970;
    tm.Month = rtc.getMonth();
    tm.Day = rtc.getDate();
    tm.Hour = rtc.getHours();
    tm.Minute = rtc.getMinutes();
    tm.Second = rtc.getSeconds();

    return makeTime(tm);
}


time_t rtc_get_local_time_t(void) {
    time_t utc = rtc_get_utc_time_t();
    return tz.toLocal(utc);
}


void rtc_set_utc_time_t(time_t t) {
    uint16_t y = year(t);
    uint16_t m = month(t);
    uint16_t d = day(t);

    rtc.updateTime();
    DBG("utc set: %04d-%02d-%02d %02d:%02d:%02d",
        y, m, d, hour(t), minute(t), second(t));
    rtc.setYear(y);
    rtc.setMonth(m);
    rtc.setDate(d);
    rtc.setWeekday(rtc_weekday_from_date(y, m, d));

    rtc.setHours(hour(t));
    rtc.setMinutes(minute(t));
    rtc.setSeconds(second(t));
}


// This could fail if called near DST start/end
void rtc_set_local_time_t(time_t t) {
    time_t utc = tz.toUTC(t);
    rtc_set_utc_time_t(utc);
}


void rtc_set_local_time(uint16_t h, uint16_t m, uint16_t s) {
    time_t t = rtc_get_local_time_t();
    tmElements_t tm;

    breakTime(t, tm);
    tm.Hour = h;
    tm.Minute = m;
    tm.Second = s;
    t = makeTime(tm);
    rtc_set_local_time_t(t);
}


boolean rtc_set_local_time(const char *str) {
    uint16_t h;
    uint16_t m;
    uint16_t s = 0;
    int n;

    // hh:mm:ss or hh:mm
    n = sscanf(str, "%2u:%2u:%2u", &h, &m, &s);
    if (n < 2 || n > 3) {
        return false;
    }

    DBG("settime: %02d:%02d:%02d", h, m, s);
    rtc_set_local_time(h, m, s);
    return true;
}


void rtc_set_local_date(uint16_t y, uint16_t m, uint16_t d) {
    time_t t = rtc_get_local_time_t();
    tmElements_t tm;

    breakTime(t, tm);
    tm.Year = y - 1970;
    tm.Month = m;
    tm.Day = d;
    t = makeTime(tm);
    rtc_set_local_time_t(t);
}

boolean rtc_set_local_date(const char *str) {
    uint16_t y;
    uint16_t m;
    uint16_t d;
    int n;

    // yyyy-mm-dd
    n = sscanf(str, "%4u-%2u-%2u", &y, &m, &d);
    if (n != 3) {
        return false;
    }
    DBG("setdate: %04d-%02d-%02d", y, m, d);
    rtc_set_local_date(y, m, d);
    return true;
}


void rtc_date_command(const char *cmd) {
    if (*cmd == '\0') {
        rtc_dbg_print_date();
        return;
    }

    // We are given either a date or a time, so we'll try both
    // and accept the first that worked.
    if (rtc_set_local_date(cmd))
        return;
    if (rtc_set_local_time(cmd))
        return;

    DBG("Bad date or time string");
}

void rtc_dbg_print_date(void) {
    time_t t = rtc_get_local_time_t();
    DBG("local: %04d-%02d-%02d %02d:%02d:%02d",
        year(t), month(t), day(t),
        hour(t), minute(t), second(t));

    t = rtc_get_utc_time_t();
    DBG("UTC:   %04d-%02d-%02d %02d:%02d:%02d",
        year(t), month(t), day(t),
        hour(t), minute(t), second(t));

    DBG("RTC:   %04d-%02d-%02d %02d:%02d:%02d (%s)",
        rtc.getYear(), rtc.getMonth(), rtc.getDate(),
        rtc.getHours(), rtc.getMinutes(), rtc.getSeconds(),
        DAYNAME(rtc.getWeekday()));
}




// Determine if we need to update the time on the display
// The RTC is set to interrupt once per minute (at start of minute).
// Therefore, if there was an RTC interrupt, clear it and return true
int rtc_new_minute(void) {
    if (rtc.getInterruptFlag(FLAG_UPDATE)) {
        rtc.clearInterruptFlag(FLAG_UPDATE);
        return true;
    }
    return false;
}

