/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <avr/sleep.h>


void sleep_init(void) {
    pinMode(PIN_RTC_INTR, INPUT);
    pinMode(PIN_RXDATA_INTR, INPUT_PULLUP);
}


/*
 * Put arduino to sleep
 * Drops current draw about 15mA
 */
void sleep(void) {
    DBG("Going to sleep");
    // Delay should not be needed after flush per
    // https://forum.arduino.cc/index.php?topic=487082.msg3324084#msg3324084
    Serial.flush();

    set_sleep_mode(SLEEP_MODE_PWR_DOWN);

    // Call wakeup() when RTC INT pin goes LOW
    enableInterrupt(PIN_RTC_INTR, func_nop, LOW);

    // Make sure serial input will trigger an interrupt
    // Note: This doesn't appear to ever cause the function to be called
    enableInterrupt(PIN_RXDATA_INTR, func_nop, CHANGE);

    sleep_mode(); // Go to sleep: sleep_enable, sleep_cpu, sleep_disable

    disableInterrupt(PIN_RXDATA_INTR);
    disableInterrupt(PIN_RTC_INTR);

    DBG("Wake up (pin=%d)", arduinoInterruptedPin);
    if (arduinoInterruptedPin == 0) {
        DBG("Serial command lost while sleeping");
        // Try to discard any partial command
        // (doesn't seem to fully work)
        //while (Serial.available())
        //    Serial.read();

        // changing global defined var in clock.ino to keep us awake longer
        next_sleep_time = millis() + STAY_AWAKE_TIME_SERIAL;
    }
}
        
