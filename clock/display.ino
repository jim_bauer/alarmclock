/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

// Sparkfun Qwiic Alphanumeric Display
// https://www.sparkfun.com/products/16427
// https://github.com/sparkfun/SparkFun_Alphanumeric_Display_Arduino_Library/
// Default I2C address is 0x70, can be changed to 0x71 by closing jumper A0
#include <SparkFun_Alphanumeric_Display.h>


// Adafruit 4-digit 7-segment display, 1.2" high
// https://www.adafruit.com/product/1264
// https://learn.adafruit.com/adafruit-led-backpack/overview
// https://github.com/adafruit/Adafruit_LED_Backpack
#include <Adafruit_GFX.h>
#include <Adafruit_LEDBackpack.h>
#define DISP_MAIN_I2C 0x72 // default is 0x70


#include "display.h"

HT16K33 disp_alpha;
Adafruit_7segment disp_main = Adafruit_7segment();

// I2C must already have been initialized
boolean disp_init(void) {
    //check if display will acknowledge
    if (disp_alpha.begin() == false) {
        DBG("Alpha display did not init\n");
        return false;
    }
    disp_alpha_set_bright(DISP_ALPHA_DEF_BRIGHT);

    disp_main.begin(DISP_MAIN_I2C);
    disp_main_set_bright(DISP_MAIN_DEF_BRIGHT);
    return true;
}


#if 0
void disp_alpha_on(void) {
    disp_alpha.displayOn();
}

void disp_alpha_off(void) {
    disp_alpha.displayOff();
}

void disp_main_on(void) {
    //disp_main.blinkRate(HT16K33_BLINK_DISPLAYON);
    disp_main.writeDisplay();
}
void disp_main_off(void) {
    disp_main.clear();
    disp_main.writeDisplay();
}
#endif


byte disp_main_bright;
byte disp_alpha_bright;

byte disp_alpha_get_bright(void) {
    return disp_alpha_bright;
}

void disp_alpha_set_bright(uint8_t val) {
    disp_alpha_bright = val;
    disp_alpha.setBrightness(val);
}

void disp_alpha_set_bright(const char *s) {
    uint8_t v = atoi(s);
    disp_alpha_set_bright(v);
}


byte disp_main_get_bright(void) {
    return disp_main_bright;
}

void disp_main_set_bright(uint8_t val) {
    disp_main_bright = val;
    disp_main.setBrightness(val);
}

void disp_main_set_bright(const char *s) {
    uint8_t v = atoi(s);
    disp_main_set_bright(v);
}


void disp_main_blink(boolean on) {
    // Valid value for blinkrate are:
    //   HT16K33_BLINK_OFF       = no blink
    //   HT16K33_BLINK_2HZ       = 2 Hz blink
    //   HT16K33_BLINK_1HZ       = 1 Hz blink
    //   HT16K33_BLINK_HALFHZ    = 0.5 Hz blink
    if (on)
        disp_main.blinkRate(HT16K33_BLINK_2HZ);
    else
        disp_main.blinkRate(HT16K33_BLINK_OFF);
    disp_main.writeDisplay();
}

void disp_alpha_blink(boolean on) {
    // Valid value for blinkrate are: 0.5, 1, & 2 HZ (all other, no blink)
    if (on)
        disp_alpha.setBlinkRate(2);
    else
        disp_alpha.setBlinkRate(0);
}


void disp_main_show_1_num(uint16_t n) {
    disp_main.drawColon(false);

    disp_main.print(n);
    disp_main.writeDisplay();
}

void disp_main_show_2_nums(uint8_t a, uint8_t b, boolean colon=false) {
    //disp_main.print(a*100 + b) will not print leading zeros

    disp_main.writeDigitNum(0, a / 10 % 10);
    disp_main.writeDigitNum(1, a % 10);
    // skip pos 2 as that is the colon and dots
    disp_main.writeDigitNum(3, b / 10 % 10);
    disp_main.writeDigitNum(4, b % 10);

    disp_main.drawColon(colon);
    disp_main.writeDisplay();
}
void disp_main_show_time(uint8_t hh, uint8_t mm) {
    disp_main_show_2_nums(hh, mm, true);
    disp_main.writeDisplay();
}

void disp_alpha_show(const char *s) {
    //DBG("Displaying: %s", s);
    disp_alpha.print(s);
}

void disp_alpha_show_time(uint8_t hh, uint8_t mm) {
    char buf[6];

    snprintf(buf, sizeof(buf), "%02d:%02d", hh, mm);
    disp_alpha_show(buf);
}

#if 0
void disp_alpha_show_2_nums(uint8_t a, uint8_t b) {
    char buf[5];
    snprintf(buf, sizeof(buf), "%02d%02d", a, b);
    disp_alpha_show(buf);
}

void disp_alpha_show_1_num(uint16_t a) {
    char buf[5];
    snprintf(buf, sizeof(buf), "%04d", a);
    disp_alpha_show(buf);
}

void disp_alpha_clear(void) {
    disp_alpha.clear();
}
#endif


void display_error(int code) {
    char buf[6];
    snprintf(buf, sizeof(buf), "E%d", code);
    disp_alpha_show(buf);
    delay(1000);
    //while (true); // hang
}

