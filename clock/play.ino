/*
 * Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/*
 * Play a little tune
 * Based on code found at https://github.com/robsoncouto/arduino-songs
 * Modified so that it call be called from loop for each note to be played.
 * Made the note specification a bit simpler.
 * Allow playing multiple melodies.
 */


#include "pitches.h"


/*
 * If connecting to standard speaker you will need a resister inline as
 * the max current for I/O pins on Arduino Uno is 40mA.
 * A simple passive piezo transducer should not need a resister,
 * but you could add one to reduce the volume.  A small 220ohm resister
 * will cut out some of the harshness of the piezo.
 * An active buzzer will need a resister.
 *
 * DPIN --- R --- speaker --- GND
 * 1000 * 5V / (Rresister + Rspeaker) < 40 mA
 *
 * 1000 * 5V / (120 ohm + 8 ohm) = 39 mA
 * 1000 * 5V / (220 ohm + 8 ohm) = 22 mA
 *
 * Or, another way, total resistance (R + speaker) must be at least
 * 1000 * 5V / 40 mA > 125 ohm
 */

/*
 * A melody is a series of notes and durations.  Some notes (negative) may be
 * special commands with an argument.
 *
 * Notes:
 * Notes are defined in pitches.h.  Do not include the NOTE_ note()
 * C4 is middle C and that is one ledger line below the standard
 * 5-lines in treble clef or one ledger line above in base clef.
 *
 * Durations:
 * A 4 means a quarter note, 8 an eighth, 16 a sixteenth, ...
 * Negative numbers are used to represent dotted notes,
 * so -4 means a dotted quarter note, i.e. a quarter plus an eighth
 *
 * Rests:
 * a rest is a note with a pitch of 0
 *
 * tempo(x)
 * Change the tempo to x bpm
 *
 * pitch(x)
 * Adjust the pitch by x octaves up (or down if negative)
 *
 * repeat_start(n) mark the start of a repeated section that
 * should be played a total on n times.  Repeats cannot be nested.
 *
 * repeat_end() mark the end of a repeated section.
 */
#define MELODY_CMD_TEMPO -1
#define MELODY_CMD_PITCH -2
#define MELODY_CMD_REPEAT_START -3
#define MELODY_CMD_REPEAT_END -4

#define note(f, d) NOTE_ ## f, d
#define rest(d) 0, d
#define tempo(t) MELODY_CMD_TEMPO, t // change tempo to t
#define pitch(p) MELODY_CMD_PITCH, p // go up/down (>0 or <0) that many octaves
#define repeat_start(c) MELODY_CMD_REPEAT_START, c
#define repeat_end() MELODY_CMD_REPEAT_END, 0

// How many beats per minute we play by default
#define DEFAULT_TEMPO 120


// Struct to hold onto current melody
struct melody {
    int *melody;
    unsigned pos; // next position to play
    unsigned len;
    int tempo;
    int pitch_adj; // <0 adjust pitch down x octaves, >1 adjust up x octaves
    unsigned repeat_pos;
    unsigned repeat_cnt;
};
struct melody melody;


#define PLAY_START(m) play_start(m, ARRAYLEN(m))
void play_start(int *_melody, unsigned len) {
    melody.melody = _melody;
    melody.len = len;
    melody.pos = 0;
    melody.tempo = DEFAULT_TEMPO;
    melody.pitch_adj = 0;
    melody.repeat_pos = 0;
    melody.repeat_cnt = 0;
}
/*
 * Plays the next note in the melody passed to play_start()
 * If we already hit the end, this will do nothing.
 * return true if we played something, else false (at end)
 */
boolean play_cont(void) {
    int divider;
    int duration = 0;
    unsigned pos;
    int op;
    int val;
    int freq;
    int wholenote;

again:
    pos = melody.pos;

    if (pos + 1 >= melody.len) {
        return false;
    }

    // the duration of a whole note in ms (60s/TEMPO)*4 beats
    wholenote = (60000 * 4) / melody.tempo;

    op = melody.melody[pos];
    val = melody.melody[pos+1];

    if (op == MELODY_CMD_TEMPO) {
        // Change tempo to given value in next position
        melody.tempo = val;
        melody.pos += 2;
        goto again;
    }

    if (op == MELODY_CMD_PITCH) {
        // Adjust pitch by number of octaves based on value in next position
        melody.pitch_adj = val;
        melody.pos += 2;
        goto again;
    }

    if (op == MELODY_CMD_REPEAT_START) {
        melody.pos += 2;
        melody.repeat_pos = melody.pos;
        melody.repeat_cnt = val - 1;
        goto again;
    }

    if (op == MELODY_CMD_REPEAT_END) {
        if (melody.repeat_cnt > 0) {
            melody.pos = melody.repeat_pos;
            melody.repeat_cnt--;
        }
        else {
            melody.pos += 2;
        }
        goto again;
    }

    // Must be a real note/rest
    freq = op;
    melody.pos += 2;

    // Make any pitch adjustment
    int adj = melody.pitch_adj;
    if (adj < 0)
        freq = freq >> -adj;
    else
        freq = freq << adj;

    // calculates the duration of each note
    // we are assuming divider can never be 0
    divider = val;
    if (divider > 0) {
        // regular note
        duration = wholenote / divider;
    }
    else { // assuming < 0
        // dotted notes are represented with negative durations
        // so we need to increase the duration by one half
        duration = wholenote / -divider;
        duration *= 1.5;
    }

    // We only play the note for 90% of the duration, but delay
    // for the full duration.  That gives us 10% of the duration time
    // as a pause/gap between notes.
    tone(PIN_BUZZER, freq, duration * 0.90);
    delay(duration);
    noTone(PIN_BUZZER);

    return true;
}


int melody_simple_beep[] = {
    // Kind of like a normal alarm beep
    tempo(120),
    repeat_start(200),
    note(A5, 8), rest(8), note(A5, 8), rest(8), note(A5, 8), rest(8),
    repeat_end(),
};

int melody_alarm_clock[] = {
    tempo(120),
    repeat_start(6),
    note(E4, 16), rest(2),
    repeat_end(),

    //tempo(140),
    repeat_start(6),
    note(E4, 16), note(E4, 16), rest(2),
    repeat_end(),

    repeat_start(6),
    note(E4, 16), note(E4, 16), note(E4, 16), rest(2),
    repeat_end(),

    repeat_start(6),
    note(E4, 16), note(E4, 16), note(E4, 16), note(E4, 16), rest(2),
    repeat_end(),

    repeat_start(40),
    note(E4, 16),
    repeat_end(),

    repeat_start(30),
    note(E4, 16), note(E6, 16),
    repeat_end(),

    repeat_start(20),
    note(E4, 8), note(E6, 8),
    repeat_end(),

    repeat_start(30),
    note(E5, 16), note(A5, 16),
    repeat_end(),

    repeat_start(20),
    note(E4, 8), note(A6, 8),
    repeat_end(),

    repeat_start(30),
    note(E4, 16), note(A6, 16),
    repeat_end(),
};


void play_alarm(void) {
    //PLAY_START(melody_simple_beep);
    PLAY_START(melody_alarm_clock);
}
