# Building this project yourself

Bewarned that these instruction are a bit minimal and are lacking some detail as there were not started until long after the project has been completed.  If anyone tries to build this, please submit any additional instructions to help others.

## Enclosure overview

The enclosure is intended to be 3D printed.   If you don't have a 3d printer or a friend with one, some libraries have them available and there are many commercial outfits that will print parts for you.  See [../CAD/README.md](../CAD/README.md) for more information.


## Electronics overview

Some soldering will be required.  In addition to the arduino, there is a 2nd circuit board in this project.  It is a place to put additional components and headers.  There is nothing special about the layout of the components on the circuit board, so you can probably come up with something better.

### Power

This projects requires 2 power supplies.  The first one powers the Arduino via the USB connector, the second provides power to the wakeup light.  You could optionally power the Arduino via the barrel connector (9V - 12V @ 2A).  The battey pack mentioned in the parts list below should only be used with the USB connector on the Arduino.

## Software overview

The software runs on an [Arduino](https://www.arduino.cc/) microcontroler.  Specifically a [Sparkfun RedBoard Qwiic](https://www.sparkfun.com/products/15123) was used.  Other similar Arduino boards could be used, but beware if the size or mounting holes locations are different.  That could necessitate changes to the enclosure.  Any reference to Arduino below is implying the Redboard Qwiic.

This project also made used the builtin Qwiic connector on the RedBoard Qwiic.  So if you use an Arduino without a Qwiic connector, you'll have to make wiring changes.

It is also not advised to use any Arduino with less memory as this is
already near the memory limits.



## Parts

The following items were used to build this project

1. [SparkFun RedBoard Qwiic](https://www.sparkfun.com/products/15123)
2. [SparkFun Real Time Clock Module - RV-8803 (Qwiic)](https://www.sparkfun.com/products/16281)
3. [Adafruit 1.2" 4-Digit 7-Segment Display w/I2C Backpack - Red](https://www.adafruit.com/product/1270)
4. [SparkFun Capacitive Touch Breakout - AT42QT1011](https://www.sparkfun.com/products/14520)
5. [Copper Foil Sheet with Conductive Adhesive - 12" x12" Sheet](https://www.adafruit.com/product/4607)
6. [Qwiic Alphanumeric Display - Red](https://www.sparkfun.com/products/16427)
7. [Piezo passive buzzer](https://www.amazon.com/gp/product/B01GJLE5BS)
    (link is for pack of 10, only 1 needed)
8. [Rocker Switch - SPST](https://www.sparkfun.com/products/11138)
9. [SparkFun Solder-able Breadboard](https://www.sparkfun.com/products/12070)
10. [Panel Mount 2.1mm DC barrel jack](https://www.adafruit.com/product/610)
11. [Momentary Push Button Switch, NO](https://www.amazon.com/gp/product/B07YC7N6Y4)
    (link is for a 10 pack, only 3 needed)
12. [24V Power Supply, 110-240V AC to DC 24Volt 1.5A](https://www.amazon.com/gp/product/B08F7DVY8G)
13. [TalentCell Rechargeable 12V 3000mAh battery pack](https://www.amazon.com/gp/product/B01M7Z9Z1N) (optional)
14. [FemtoBuck LED Driver](https://www.sparkfun.com/products/13716)
15. [LED - 3W Aluminum PCB (5 Pack, Warm White)](https://www.sparkfun.com/products/13104)
16. [SparkFun Qwiic Cable Kit](https://www.sparkfun.com/products/15081)
17. [1/8" x 6" x 12" Grey "Smoked" Plexiglass sheet](https://www.estreetplastics.com/Plexiglass-Grey-Smoked-1-8-x-6-x-12-p/t811250612.htm)
18. [Frosted Plexiglass 3/16" thick 6" x 12"](https://www.estreetplastics.com/Frosted-Plexiglass-3-16-x-6-x-12-p/1201870612.htm)
19. [1 in x 36 in. Aluminum flat bar](https://www.homedepot.com/p/Everbilt-1-in-x-36-in-Aluminum-Flat-Bar-with-1-8-in-Thick-801937/204273949)
    (only need about 5 inches)
20. 1k ohm resister
21. 220 ohm resister
22. 100 uF capacitor

Other items that may also needed

- [Hook-up Wire Spool Set - 22AWG Solid Core - 6 x 25 ft](https://www.adafruit.com/product/1311)
- [Hook-Up Wire - Assortment (Stranded, 22 AWG)](https://www.sparkfun.com/products/11375)
- Soldering iron
- Solder
- M3 screws (not bolts)
- Heat sink thermal tape (or hot glue gun)
- Wire nuts
- Zip ties


## Wiring

(click images for full-sized image)


[![](../images/alarmclock_schem.png){width=700}](../images/alarmclock_schem.png)  
Schematic

[![](../images/alarmclock_bb.png){width=700}](../images/alarmclock_bb.png)  
Breadboard
<br>
<br>

| [![](../images/electronics_overview.jpg){width=300}](../images/electronics_overview.jpg) | [![](../images/breadboard.jpg){width=300}](../images/breadboard.jpg) | [![](../images/femtobuck.jpg){width=300}](../images/femtobuck.jpg) |
|:---:|:---:|:---:|
| Electronics overview | Solderable breadboard | Femtobuck |
| | | |
| [![](../images/displays.jpg){width=300}](../images/displays.jpg) | [![](../images/ledmount.jpg){width=300}](../images/ledmount.jpg) | [![](../images/touch.jpg){width=300}](../images/touch.jpg) |
|  Displays | LED mount | Capacitive Touch |

<br>


The orange item in some of the photos is just a Wago lever nut.  Standard wire nuts can be used instead.  The white item joining some wires is similar.




## Assembly

Inside the enclosure there are mounting posts for the RedBoard and a solderable breadboard.  M3 screws should work to screw into the holes in the mounting posts.

As an alternative to the solderable breadboard, you could remove the mounting posts from the enclosure and place a standard breadboard in the enclosure using an adhesive backing to secure it in place.  However connections will not be as reliable.  Other connections will still need soldering.

The displays are mounted into the front panel with screws.  The FemtoBuck is mounted on the back panel with zip ties.  The aluminum bar with the LEDs is also mounted with zip ties.

The LEDs should be mounted to the aluminum bar (centered in width, evenly spread out along length) with heat sink thermal tape.  Hot glue will work, but it will soften due to the heat from the LEDs, but it is not the best choice.  A piece of clear frosted plexiglass 2/16" think need to be cut to fit into the slot.  A circular saw is good for cutting, be sure to keep the protective film on it while cutting.  The frosted plexiglass should rest directly on the LEDs so that the LEDs shine into the edge which will light up the whole panel.


## Compile and upload software


- Install the [Arduino IDE](https://www.arduino.cc/en/software)
- Plug in the Arduino (Sparkfun Redboard Qwiic)
- Run the Arduino IDE and open the clock/clock.ino file.
- Set Tools->Port to the correct device
- Set Tools->Board to "Arduino UNO"
- Install needed libraries
    - Enableinterrupt
    - Time
    - SparkFun Qwiic Alphanumeric Display
    - Sparkfun Qwiic RTC RV8803
    - Adafruit GFX
    - Adafruit LED Backpack
    - arduino-timer
- Edit config.h file as described below
- Click Upload (Right arrow in tool bar) to compile and upload program

### Software Configuration

Timezone.  You will need to edit clock/config.h and change the timezone rules to match your local timezone.  The default is set to US/Eastern.  If you don't observe Daylight Saving Time where you are, Just set dstRule and stdRule to the same vaules.

