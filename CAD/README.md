# CAD drawings


![case rendering](../images/box.png)


![open case rendering](../images/box-no-top.png)



## Running openscad

To run openscad on the box.scad file, run in this directory

```
./runopenscad [options] <file.scad>
```


## 3D Printing

You need to generate .stl files via ```make stl``` and have them
printed with your own 3D printer or via a 3D printing service.  For a
list of some 3d printing services, see
[https://all3dp.com/1/best-online-3d-printing-service-3d-print-services/](https://all3dp.com/1/best-online-3d-printing-service-3d-print-services/)

### 3D Print Settings

- Rafts: None
- Supports: yes (top & front), no (bottom & back)
- Resolution: 0.2mm
- Infill: 30% (in retrospect, 15% is probably more than enough)

I used PLA, but in retrospect something like PETG would be a better option.
The heat the the high powered LEDs did deform the PLA a bit.


## Subtrees

box.scad depends on a couple of external modules that have been added
as git subtrees.  You should not need to do any of this, but they were
setup as follows:

```
git remote add scad-arduino https://github.com/KjellMorgenstern/OpenSCAD-Arduino-Mounting-Library.git
git remote add scad-pins https://github.com/tbuser/pin_connectors.git

git subtree add --prefix=CAD/libs/arduino scad-arduino master --squash
git subtree add --prefix=CAD/libs/pin_connectors scad-pins master --squash
```

To pull in later changes

```
git subtree pull --prefix=CAD/libs/arduino scad-arduino master --squash
git subtree pull --prefix=CAD/libs/pin_connectors scad-pins master --squash
```


## Credits

The box.scad file originally came from
[https://www.thingiverse.com/thing:1264391](https://www.thingiverse.com/thing:1264391) file U_Box_V104_Test_Cleaned.scad on 2020-12-03

License: [Creative Commons - Attribution - Non-Commercial](https://creativecommons.org/licenses/by-nc/4.0/)

### Libraries

#### libs/arduino

Taken from: [https://github.com/KjellMorgenstern/OpenSCAD-Arduino-Mounting-Library.git](https://github.com/KjellMorgenstern/OpenSCAD-Arduino-Mounting-Library.git) on 2021-08-21

License: MIT

#### libs/pin_connectors

Taken from [https://github.com/tbuser/pin_connectors](https://github.com/tbuser/pin_connectors) on 2021-08-21
also at [https://www.thingiverse.com/thing:10541/files](https://www.thingiverse.com/thing:10541/files)

License: [Creative Commons - Attribution - Share Alike](https://creativecommons.org/licenses/by-sa/3.0/) based on a link at [https://github.com/tbuser/pin_connectors](https://github.com/tbuser/pin_connectors) (About Information) pointing to [https://www.thingiverse.com/thing:10541](https://www.thingiverse.com/thing:10541)
