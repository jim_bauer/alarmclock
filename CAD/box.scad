//
// SPDX-License-Identifier: CC-BY-NC-3.0
//

/*
 * Original version from https://www.thingiverse.com/thing:1264391
 *   FB Aka Heartman/Hearty 2016
 *   http://heartygfx.blogspot.com
 *   OpenScad Parametric Box
 *   License: CC BY-NC 3.0 License
 */

/*
2016-02-12 - Fixed minor bug
2016-02-28 - Added holes ventilation option
2016-03-09 - Added PCB feet support, fixed the shell artefact on export mode.

2020-12-03 - Incorporate into alarmclock project
2020-12-04 - Added multiple PCB support from comment by zafrirron (2020-11-13)
             https://www.thingiverse.com/thing:1264391/comments
2020-12-04 - Added change to feet positioning from comment by VeeDubb65 (2016-11-27)
             https://www.thingiverse.com/thing:1264391/comments
*/


include <arduino/arduino.scad>
include <display.scad>
use <screws.scad>
use <light-panel.scad>
use <light-tube.scad>


// Info
// - The front/back thickness is Thick/2.
// - The top/bottom shell thickness is Thick in most places and Thick*2
// in attachment areas.
// - The inside edge of the front/back panel is Thick*2 (plus a wee bit)
// from the outer edge of the shell.


// All coordinates are starting as integrated circuit pins.
// From the top view :
//   CoordD           <---       CoordC
//                                 ^
//                                 ^
//   CoordA           --->       CoordB


// - Box parameters -

/* [Box dimensions] */
// - Length (front-to-back)
Length        = 150;
// - Width (side-to-side)
Width         = 160;
// - Height (top-to-bottom)
Height        = 100;
// - Wall thickness
Thick         = 2; //[1:5]


// If you play with the Fillet value you will go from the sharp to the
// round sides box.  If you want a chanfered box, set Resolution to 1.

/* [Box options] */
// - Fillet diameter
Fillet         = 2; //[0.1:12]
// - Fillet smoothness
Resolution    = 1; //[1:100]
// - Tolerance (Panel/rails gap)
// https://3dchimera.com/blogs/connecting-the-dots/3d-printing-tolerances-fits
m             = 0.508;
// - Decorations to ventilation holes
Vent          = 1; // [0:No, 1:Yes]
// - Decoration-Holes width (in mm)
Vent_width    = 1.5;
// - Add Light Panel
Light_panel   = 1;  // [0:No, 1:Yes]
// - Add Light Tube
Light_tube    = 0;  // [0:No, 1:Yes]


/* [PCB_Feet] */
// - PCB feet
PCBFeet         = 1;// [0:No, 1:Yes]
// Length and width are distance between hole centers
// [[x, y,  l, w, h,  dia, holeDia, desc], [...] ]
PCBs = [
    // Lower left
    // hole         Hole spacing     Foot
    // ----------   ------------     ----------------
    // x,  y,       l,      w,       h, dia,  holeDia,  desc
    // PTH protoboard-30 (hole spacing from CAD)
    [ 17,  85,      86.36,  55.88,  10,   6,     2.8,     "PTH-30"],
    ];

/* [Cross sections] */
// Left
cross_left = 0;
// Right
cross_right = 0;
// Top
cross_top = 0;
// Bottom
cross_bottom = 0;
// Front
cross_front = 0;
// Back
cross_back = 0;



/* [STL elements to export] */
// Top shell
TShell          = 0; // [0:No, 1:Yes]
// Bottom shell
BShell          = 1; // [0:No, 1:Yes]
// Front panel
FPanL           = 1; // [0:No, 1:Yes]
// Back panel
BPanL           = 1; // [0:No, 1:Yes]



/* [Hidden] */
color_shell       = "Orange";
color_panel_text  = "Orange";
color_front_panel = "OrangeRed";
color_back_panel  = "Coral";
color_pcb         = "PowderBlue";
color_pcb_text    = "SteelBlue";
color_buttons     = "Black";
color_jacks       = "Black";
color_tiedown     = "Red";
color_version     = "Yellow";

// Thick X 2 - making decorations thicker if it is a vent to make sure they go through shell
Dec_Thick       = Vent ? Thick*2 : Thick;
// - Depth decoration
Dec_size        = Vent ? Thick*2 : 0.8;
// How close to the front/back to start
Dec_start       = 20;
// How long each section is
Dec_end         = Length / 3;

default_font = "Liberation Sans:style=Bold";
Version = "Unknown"; // Set to date by runopenscad

// Push button switch
// https://smile.amazon.com/dp/B07YC7N6Y4
// Pin Pitch: 3mm / 0.11".
// Cap Diameter: 6mm / 0.24".
// Thread Diameter: 6.9mm / 0.27".
// Overall Size: 25 x 9mm / 0.98" x 0.35" (Max. L * D).
button1_pos = [ 25, 24 ];
button2_pos = [ 40, 24 ];
button3_pos = [ 70, 24 ];
button_base_dia = 9; //mm
button_base_len = 2;
button_base_recess = 1; // how deep to recess button into panel
button_screw_dia = 7;
button_inside_len = 15; // measured
button_outside_len = 12; // measured
button_inside_clearence = 35 - button_inside_len; // room for connectors
button_len = button_inside_len + button_outside_len;


// Enable these to have plexi behind front panel
plexi_thick = 2.83; // plexi thickness (measured)
plexi_xnotch = 3; // xtra width of plexi
plexi_ynotch = 3; // xtra height of plexi notched into display frame
// Enable these to have plexi in front of front panel (will not be shown)
//plexi_thick = 0;
//plexi_ynotch = 0;
//plexi_xnotch = 0;


// Convert inches to mm
function inch2mm(inch) = inch * 25.4;


// - Generic rounded box
module RoundBox($a=Length, $b=Width, $c=Height){// Cube bords arrondis
    $fn=Resolution;
    translate([0,Fillet,Fillet]){
        minkowski (){
            cube ([$a-(Length/2),$b-(2*Fillet),$c-(2*Fillet)], center = false);
            rotate([0,90,0]){
                cylinder(r=Fillet,h=Length/2, center = false);
            }
        }
    }
}

// Top or bottom shell
module shell() {
    Thick = Thick*2;
    difference(){
        union(){
            difference() { // Substraction Filleted box
                union() {
                    difference(){ // shell
                        RoundBox();
                        // Hollow out box
                        translate([Thick/2,Thick/2,Thick/2]){
                            RoundBox($a=Length-Thick, $b=Width-Thick, $c=Height-Thick);
                        }
                    }

                    // Add in inner rails
                    difference(){
                        translate([Thick+m,Thick/2,Thick/2]){// Rails
                            RoundBox($a=Length-((2*Thick)+(2*m)), $b=Width-Thick, $c=Height-(Thick*2));
                        }
                        // +0.1 added to avoid the artefact
                        translate([((Thick+m/2)*1.55),Thick/2,Thick/2+0.1]){
                            RoundBox($a=Length-((Thick*3)+2*m), $b=Width-Thick, $c=Height-Thick);
                        }
                    }
                } // end union

                // cube top (or bottom) to remove
                translate([-Thick,-Thick,Height/2]){ // Cube to subtrack
                    cube ([Length+100, Width+100, Height], center=false);
                }

                // Remove front/back of cube
                translate([-Thick/2,Thick,Thick]){ // Central subtracting form
                    RoundBox($a=Length+Thick, $b=Width-Thick*2, $c=Height-Thick);
                }
            }

            // Add top/bottom attachment legs
            difference(){// wall fixation box legs
                union(){
                    translate([3*Thick +5,Thick,Height/2]){
                        rotate([90,0,0]){
                            $fn=6;
                            cylinder(d=16,Thick/2);
                        }
                    }

                    translate([Length-((3*Thick)+5),Thick,Height/2]){
                        rotate([90,0,0]){
                            $fn=6;
                            cylinder(d=16,Thick/2);
                        }
                    }

                }

                // Remove part of attachment tabs
                translate([4,Thick+Fillet,Height/2-57]){
                    rotate([45,0,0]){
                        cube([Length,40,40]);
                    }
                }
                translate([0,-(Thick*1.46),Height/2]){
                    cube([Length,Thick*2,10]);
                }
            } // end fixation box legs
        }

        union() { // remove side vents/decorations
            for(i=[Dec_start:Thick:Dec_end]){
                // Ventilation holes part code submitted by Ettie - Thanks ;)
                translate([i, -Dec_Thick+Dec_size, 1]){
                    cube([Vent_width,Dec_Thick,Height/4]);
                }
                translate([Length - i, -Dec_Thick+Dec_size, 1]){
                    cube([Vent_width,Dec_Thick,Height/4]);
                }
                translate([Length - i, Width-Dec_size, 1]){
                    cube([Vent_width,Dec_Thick,Height/4]);
                }
                translate([i, Width-Dec_size, 1]){
                    cube([Vent_width,Dec_Thick,Height/4]);
                }
            }
        }

        //side screw holes
        union(){
            $fn=50;
            translate([3*Thick+5,20,Height/2+4]){
                rotate([90,0,0]){
                    cylinder(d=2,20);
                }
            }
            translate([Length-((3*Thick)+5),20,Height/2+4]){
                rotate([90,0,0]){
                    cylinder(d=2,20);
                }
            }
            translate([3*Thick+5,Width+5,Height/2-4]){
                rotate([90,0,0]){
                    cylinder(d=2,20);
                }
            }
            translate([Length-((3*Thick)+5),Width+5,Height/2-4]){
                rotate([90,0,0]){
                    cylinder(d=2,20);
                }
            }
        }

    } // end of difference holes
}



// Foot with base fillet
// Position is center of foot bottom
module foot(FootDia, FootHole, FootHeight) {
    Fillet=2;
    color(color_shell) {
        difference() {
            cylinder(d=FootDia+Fillet, FootHeight, $fn=100);

            // Make foot flared out at bottom
            rotate_extrude($fn=100) {
                translate([(FootDia+Fillet*2)/2, Fillet, Thick] ){
                    minkowski() {
                        square(FootHeight);
                        circle(Fillet, $fn=100);
                    }
                }
            }
            // Remove screw hole
            cylinder(d=FootHole,FootHeight+0.1, $fn=100);
        }
    }
}


// Place feet for a board
// Position should be at center of lower left hole
module PCB_Feet(length, width, height, fdia, fhole, desc){
    xtra_overlap = 4;  // Extend board past hole this much in each direction

    // Place board (only visible in the preview mode)
    board_xtra = fdia + xtra_overlap * 2;
    board_length = length + board_xtra;
    board_width = width + board_xtra;
    // Move to center of board
    translate([length/2, width/2, height]) {
        color(color_pcb)
            %square([board_length, board_width], center=true);

        // Add description, just a wee bit higher
        translate([0, 0, 0.5]) {
            color(color_pcb_text)
                %text(desc, size=5,
                      halign="center", valign="center",
                      font=default_font);
        }
    }

    // 4 Feet
    translate([0, 0, 0]) {
        foot(fdia, fhole, height);
    }
    translate([length, 0, 0]) {
        foot(fdia, fhole, height);
    }
    translate([length, width, 0]) {
        foot(fdia, fhole, height);
    }
    translate([0, width, 0]) {
        foot(fdia, fhole, height);
    }
}


module chamfer(cut, length, axis, trans) {
    e = 0.01;
    assert(axis == "x" || axis == "y" || axis == "z" , "Invalid dir");

    // Rotate to desired axis and always rotate cube 45 for cut
    rot =
        (axis == "x") ? [45, 0, 0] :
        (axis == "y") ? [45, 0, 90] :
        [45, 90, 90];

    // Translate to center of edge
    half_trans =
        (axis == "x") ? [length/2, 0, 0] :
        (axis == "y") ? [0, length/2, 0] :
        [0, 0, length/2];

    translate(half_trans) {
        translate(trans) {
            rotate(rot) {
                cube([length + 2*e, cut, cut], center=true);
            }
        }
    }
}


// Create base panel (front or back)
module Panel(width, height, thick) {
    if (Resolution == 1) { // Chamfer the corners
        cut = sqrt(2 * (2*Fillet)^2);
        difference() {
            size = [thick, width+Fillet*2, height+Fillet*2];
            cube(size);
            chamfer(cut, size[0], "x", [0, 0,       0]);       //bottom front
            chamfer(cut, size[0], "x", [0, size[1], 0]);       //bottom back
            chamfer(cut, size[0], "x", [0, size[1], size[2]]); //top back
            chamfer(cut, size[0], "x", [0, 0,       size[2]]); //top front

        }
    }
    else { // round corners
        scale([0.5,1,1])
            minkowski() {
            cube([thick, width, height]);
            translate([0, Fillet, Fillet])
                rotate([0, 90, 0])
                cylinder(r=Fillet, h=thick, $fn=Resolution);
        }
    }
}


// Square hole
// Sx=Square X position | Sy=Square Y position |
// Sl=Square Length | Sw=Square Width | Fillet = Round corner
module SquareHole(Sx, Sy, Sl, Sw, Fillet){
    minkowski() {
        translate([Sx+Fillet/2, Sy+Fillet/2, -1]) {
            cube([Sl-Fillet, Sw-Fillet, 10]);
        }
        cylinder(d=Fillet, h=10, $fn=100);
    }
}


// Rocker Switch - SPST (round)
// https://www.sparkfun.com/products/11138
// 20mm dia hole, with notch on left side
rocker_switch_dia = 20;
rocker_notch_width = 1; // measured
rocker_notch_height = 2; // measured
rocker_switch_ring_dia = 23;
rocker_switch_ring_depth = 2; // measured
rocker_switch_depth = 17.8;
rocker_switch_conn_depth = 25.7 - rocker_switch_depth;
module rocker_switch_hole(x, y) {
    translate([x, y, -1]) {
        cylinder(d=rocker_switch_dia, 10, $fn=50);
    }

    notch_x = x + rocker_switch_dia / 2 - 0.2;
    notch_y = y - rocker_notch_height / 2;
    SquareHole(notch_x, notch_y,
               rocker_notch_width + 0.2, rocker_notch_height,
               0);
}
module rocker_switch(x, y) {
    translate([x, y, -rocker_switch_ring_depth+0.1]) {
        color(color_jacks, 0.5) {
            %cylinder(d=rocker_switch_ring_dia,
                      h=rocker_switch_ring_depth, $fn=50);
            %cylinder(d=rocker_switch_dia,
                      h=rocker_switch_depth, $fn=50);
        }
        color(color_jacks, 0.2) {
            translate([0, 0, rocker_switch_depth]) {
                %cylinder(d=rocker_switch_dia,
                      h=rocker_switch_conn_depth, $fn=50);
            }
        }
    }
}

// Panel mount 2.1mm DC barrel jack
// https://www.adafruit.com/product/610
// Position (x,y) is at center of opening
//dc_barrel_width = inch2mm(0.472); // datasheet
//dc_barrel_dia = inch2mm(0.512); // datasheet
//dc_barrel_outside_ring_dia = inch2mm(0.630); // datasheet
dc_barrel_width = inch2mm(0.442); // measured
dc_barrel_dia = inch2mm(0.482); // measured
dc_barrel_outside_ring_dia = inch2mm(0.540); // measured
dc_barrel_outside_ring_depth = inch2mm(0.079);
dc_barrel_depth = inch2mm(.492) + inch2mm(0.047);
dc_barrel_conn_depth = inch2mm(0.177);
module dc_barrel_jack_hole(x, y) {
    height = Thick*2.5;
    translate([x, y, -0.2]) {
        intersection() {
            cylinder(d=dc_barrel_dia, h=height, $fn=50);
            cube([dc_barrel_width, dc_barrel_dia, height], center=true);
        }
    }
}
module dc_barrel_jack(x, y) {
    translate([x, y, -dc_barrel_outside_ring_depth+0.1]) {
        color(color_jacks, 0.5) {
            %cylinder(d=dc_barrel_outside_ring_dia,
                      h=dc_barrel_outside_ring_depth, $fn=50);
            %cylinder(d=dc_barrel_dia,
                      h=dc_barrel_depth, $fn=50);
        }
        color(color_jacks, 0.25) {
            translate([0, 0, dc_barrel_depth]) {
                %cylinder(d=dc_barrel_dia,
                      h=dc_barrel_conn_depth, $fn=50);
            }
        }
    }
}


// Femtobuck tiedown
// https://www.sparkfun.com/products/13716
// Position (x,y) is center, z is front surface of panel
module femtobuck_tiedown(x, y) {
    length = 37;
    width = 7.7;
    height = 6;
    hole_dia = 3;
    hole_from_end = 2;
    pin_slot_from_edge = 7;
    pin_depth = 2;
    pin_width = 2;

    translate([x, y, Thick+height/2-0.1]) {
        difference() {
            cube([length, width, height], center=true);

            // Remove notch for pins under board
            pin_delta = length/2 - pin_slot_from_edge;
            translate([pin_delta, 0, height/2 - pin_depth/2 + 0.1]) {
                cube([pin_width, width+1, pin_depth], center=true);
            }

            // Remove holes for zip-ties
            rotate([90, 0, 0]) {
                hole_delta = length/2 - hole_from_end - hole_dia/2;
                translate([-hole_delta, 0, -width]) {
                    cylinder(d=hole_dia, h=width*2);
                }
                translate([hole_delta, 0, -width]) {
                    cylinder(d=hole_dia, h=width*2);
                }
            }
        }
    }
}

module version_text(pos) {
    color(color_version) {
        translate(pos) {
            linear_extrude(height=0.6) {
                text(Version, size=5, font=default_font);
            }
        }
    }
}

// Text inset into panel
// For front panel depth should be >0, for back panel <0
//
// Letter line thickness with default uppercase bold font
//    size  thickness(approx)
//     3       0.5mm
//     4       0.75mm
//     5       1.0mm
//     6       1.3mm
module inset_text(pos, depth, size, string) {
    height = (depth<0) ? abs(depth) + 0.01 : depth + 0.01;

    translate([pos[0], pos[1], depth]) {
        linear_extrude(height=height) {
            text(string, size=size, font=default_font,
                 halign="center", valign="center");
        }
    }
}


module button(pos) {
    // Main part of button
    // Position is center of inside surface
    translate([0, 0, button_base_recess]) {
        color(color_buttons, 0.5) {
            translate([pos[0], pos[1], -button_inside_len]) {
                %cylinder(d=button_screw_dia, button_len, $fn=50);
            }
        }
        // Inside base
        color(color_buttons, 0.5) {
            translate([pos[0], pos[1], - button_base_len]) {
                %cylinder(d=button_base_dia, button_base_len, $fn=50);
            }
        }
        // Outside nut (use inside base sizes)
        color(color_buttons, 0.5) {
            translate([pos[0], pos[1], Thick]) {
                %cylinder(d=button_base_dia, button_base_len, $fn=50);
            }
        }
        color(color_buttons, 0.2) {
            translate([pos[0], pos[1],
                       -(button_inside_len + button_inside_clearence)]) {
                %cylinder(d=button_screw_dia, button_inside_clearence, $fn=50);
            }
        }
    }
}


module button_hole(pos) {
    // Position is center of inside surface
    translate([pos[0], pos[1], -1]) {
        cylinder(d=button_screw_dia, h=Thick+2, $fn=50);
        // Add recess for base to allow more threads to come out the front
        cylinder(d=button_base_dia+0.5, 2, $fn=50);
    }
}


module plexi(x, y, z, w, h, d) {
    translate([x, y, z]) {
        color("sienna", 0.4) {
            %cube([w, h, d]);
        }
    }
}


panel_width = Width - (Thick*2 + Fillet*2 + m);
panel_height = Height - (Thick*2 + Fillet*2 + m);


// Front module Panel
// At start the origin is Lower-Left-Back corner of panel viewed from front
module FPanL(){
    panel_x = Fillet; // from Panel()
    d1_w = display_disp_width(DISPLAY_ADAFRUIT_12);
    d1_h = display_disp_height(DISPLAY_ADAFRUIT_12);
    d1_x = panel_x + (panel_width / 2) - (d1_w / 2);
    d1_y = 40;
    d1_right = d1_x + d1_w;

    d2_w = display_disp_width(DISPLAY_QWIIC_ALPHA);
    d2_h = display_disp_height(DISPLAY_QWIIC_ALPHA);
    d2_x = d1_right - d2_w; // Align right edge with d1
    d2_y = 12;

    min_lip_thick = 1; // Min panel thickness to keep at display notches
    notch_depth = Thick - min_lip_thick;
    disp_z = notch_depth - plexi_thick;

    difference() {
        union() {
            color(color_front_panel) {
                Panel(panel_width, panel_height, Thick);
            }

            rotate([90, 0, 90]) {
                // Main display
                translate([d1_x, d1_y, disp_z]) {
                    translate(display_digits_to_origin(DISPLAY_ADAFRUIT_12)) {
                        display_frame(DISPLAY_ADAFRUIT_12,
                                      zxtra=plexi_thick-notch_depth+0.1,
                                      color=color_front_panel,
                                      screw_name="M2", screw_len=4);
                        translate([0,0,-0.01]) {
                            %display(DISPLAY_ADAFRUIT_12);
                        }
                    }

                    if (plexi_thick) {
                        plexi(-plexi_xnotch, -plexi_ynotch, 0,
                              d1_w + plexi_xnotch*2,
                              d1_h + plexi_ynotch*2,
                              plexi_thick);
                    }
                }

                // Small display
                translate([d2_x, d2_y, disp_z]) {
                    translate(display_digits_to_origin(DISPLAY_QWIIC_ALPHA)) {
                        display_frame(DISPLAY_QWIIC_ALPHA,
                                      zxtra=plexi_thick-notch_depth+0.1,
                                      color=color_front_panel,
                                      screw_name="M3", screw_len=4);
                        translate([0,0,-0.01]) {
                            %display(DISPLAY_QWIIC_ALPHA);
                        }
                    }

                    if (plexi_thick) {
                        plexi(-plexi_xnotch, -plexi_ynotch, 0,
                              d2_w + plexi_xnotch*2,
                              d2_h + plexi_ynotch*2,
                              plexi_thick);
                    }
                }
            }
        }

        rotate([90,0,90]){
            color(color_front_panel){
                button_hole(button1_pos);
                button_hole(button2_pos);
                button_hole(button3_pos);

                // Adafruit 1.2" 4-Digit 7-Segment Display w/I2C Backpack
                // https://www.adafruit.com/product/1270
                // https://learn.adafruit.com/assets/36299
                SquareHole(d1_x, d1_y, d1_w, d1_h, 0);

                // Sparkfun Qwiic Alphanumeric Display
                // https://www.sparkfun.com/products/16427
                SquareHole(d2_x, d2_y, d2_w, d2_h, 0);
            }

            inset_text([button1_pos[0], button1_pos[1] - button_base_dia * 1.2],
                       depth=Thick-0.6, size=10, string="-");
            inset_text([button2_pos[0], button2_pos[1] - button_base_dia * 1.2],
                       depth=Thick-0.6, size=10, string="+");
            inset_text([button3_pos[0], button3_pos[1] - button_base_dia * 1.2],
                       depth=Thick-0.6, size=6, string="MODE");

            // Remove notch for plexiglass, plus extra to slide it into place
            // Main display
            translate([d1_x - plexi_xnotch,
                       d1_y - plexi_ynotch,
                       -(plexi_thick+0.1)]) {
                cube([d1_w + plexi_xnotch*2,
                      d1_h + plexi_ynotch*2,
                      plexi_thick + notch_depth]);
            }

            // Small display
            translate([d2_x - plexi_xnotch,
                       d2_y - plexi_ynotch,
                       -(plexi_thick+0.1)]) {
                cube([d2_w + plexi_xnotch*2,
                      d2_h + plexi_ynotch*2,
                      plexi_thick + notch_depth]);
            }
        }
    }

    // Buttons
    rotate([90,0,90]) {
        button(button1_pos);
        button(button2_pos);
        button(button3_pos);
    }

    rotate([90, 0, -90]) {
        version_text([-90, 12, 0]);
    }
}


// Back panel
module BPanL() {
    left_hole_x = Width - 35;

    difference() {
        color(color_back_panel) {
            Panel(panel_width, panel_height, Thick);
        }

        color(color_back_panel) {
            rotate([90,0,90]) {
                // Cut out shapes
                //  (Xpos,Ypos,Length,Width,Fillet)
                SquareHole(40.25, 9, 14, 15, 1); // Arduino Uno USB
                SquareHole(12, 10, 11, 13, 1); // Arduino Uno power

                rocker_switch_hole(left_hole_x, 72); // Alarm on/off switch
                dc_barrel_jack_hole(left_hole_x, 30); // Power for light
            }
        }

        rotate([90, 0, -90]) {
            inset_text([-left_hole_x, 52], depth=-0.6, size=4,
                       string="ALARM ON/OFF");
            inset_text([-left_hole_x, 15], depth=-0.6, size=4,
                       string="LIGHT POWER");
        }
    }

    rotate([90, 0, 90]) {
        // These need to be the same location as the "holes" above
        rocker_switch(left_hole_x, 72);
        dc_barrel_jack(left_hole_x, 30);

        color(color_tiedown) {
            femtobuck_tiedown(100, 50);
        }
    }

    rotate([90, 0, 90]) {
        version_text([100, 15, Thick]);
    }
}


module top_shell() {
    light_x = Light_panel ? Length / 3 : Length / 3 + 15;
    light_y = Width / 2;
    light_z = Height - Thick + 0.2;

    difference() {
        translate([0,Width,Height+0.2]){
            rotate([0,180,180]){
                color(color_shell) {
                    shell();
                }
                version_text([100, 15, Thick]);
            }
        }
        if (Light_panel)
            light_panel_hole(light_x, light_y, light_z);
        if (Light_tube)
            light_tube_hole(light_x, light_y, light_z);
    }

    translate([0, 0, 0]) {
        if (Light_panel) {
            light_panel_holder(light_x, light_y, light_z);
            %light_panel(light_x, light_y, light_z);
        }
        if (Light_tube) {
            light_tube_holder(light_x, light_y, light_z);
            %light_tube(light_x, light_y, light_z);
        }
    }
}


module bottom_shell() {
    color(color_shell) {
        shell();
    }

    version_text([100, 15, Thick]);

    if (PCBFeet == 1) {
        for (pcb = PCBs) {
            translate([pcb[0], pcb[1], Thick-0.1]) {
                //       length, width,  height, footdia, holedia, desc
                PCB_Feet(pcb[2], pcb[3], pcb[4], pcb[5], pcb[6], pcb[7]);
            }
        }

        // Add an arduino
        // The default ardiono orientation is that the edge of the
        // *board* with the power and USB connectors is along the
        // X-axis (y=0). The corner near the USB connector is the
        // origin.  The board bottom is at 0 on the z-axis
        arduino_height = 10;
        translate([6, 65, Thick-0.1]) {
            rotate([0, 0, -90]) {
                // mountType is either PIN, TAPHOLE, or NOTCH
                color(color_shell)
                    standoffs(height=arduino_height, mountType=TAPHOLE);

                translate([0, 0, arduino_height])
                    %arduino(UNO);
            }
        }
    }
}


module box() {
    if (TShell == 1) {
        top_shell();
    }

    if(BShell == 1) {
        bottom_shell();
    }

    // Front panel
    if(FPanL==1) {
        translate([Length-(Thick*2+m/2),Thick+m/2,Thick+m/2])
            FPanL();
    }

    // Back panel
    if(BPanL==1) {
        translate([Thick+m/2,Thick+m/2,Thick+m/2])
            BPanL();
    }
}


difference() {
    box();

    // Optionally remove a cross section (for debugging)
    if (cross_left) {
        translate([-1, 0, -1]) {
            cube([Length+2, cross_left, Height+2]);
        }
    }

    if (cross_right) {
        translate([-1, Width-cross_right, -1]) {
            cube([Length+2, Width, Height+2]);
        }
    }

    if (cross_top) {
        translate([-1, 0, Height-cross_top]) {
            cube([Length+2, Width, Height]);
        }
    }

    if (cross_bottom) {
        translate([-1, 0, -1]) {
            cube([Length+2, Width, cross_bottom]);
        }
    }

    if (cross_front) {
        translate([Length-cross_front, 0, -1]) {
            cube([cross_front, Width, Height+2]);
        }
    }

    if (cross_back) {
        translate([-1, 0, -1]) {
            cube([cross_back, Width, Height+2]);
        }
    }
}

