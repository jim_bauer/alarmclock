//
// Dual licensed
// SPDX-License-Identifier: CC-BY-NC-3.0
// SPDX-License-Identifier: CC-BY-SA-4.0
// SPDX-License-Identifier: GPL-3.0-or-later
//


function inch2mm(inch) = inch * 25.4;

// These are only used when testing this standalone
Thick = 2;
_top_x_len = 150; // front-to-back
_top_y_len = 200; // side-to-side
_top_thick = 2;
_light_tube_pos_x = _top_x_len / 3 + 15;
_light_tube_pos_y = _top_y_len / 2;


light_tube_od = inch2mm(4);
light_tube_id = inch2mm(3 + 7/8);
light_tube_depth = 20;
light_tube_height = inch2mm(12);
light_tube_hole_dia = 10;


module light_tube_hole(x, y, z) {
    // Move to center of tube and down a bit less to
    // make space for the actual tube
    translate([x, y, z -light_tube_depth]) {
        cylinder(d=light_tube_od, h=light_tube_depth + Thick*2);
    }
}


module light_tube_holder(x, y, z) {
    $fn=100;
    difference() {

        // Move to center of tube and down
        translate([x, y, z -light_tube_depth - Thick]) {
            cylinder(d=light_tube_od+Thick, h=light_tube_depth + Thick);
        }
        

        // Move to center of tube and down a bit less to
        // make space for the actual tube
        translate([x, y, z -light_tube_depth]) {
            cylinder(d=light_tube_od, h=light_tube_depth + Thick*2);
        }

        hole_shift = light_tube_od/2 - light_tube_hole_dia;
        translate([x+hole_shift, y, z -light_tube_depth - Thick*2]) {
            cylinder(d=light_tube_hole_dia, h=light_tube_depth);
        }
        translate([x-hole_shift, y, z -light_tube_depth - Thick*2]) {
            cylinder(d=light_tube_hole_dia, h=light_tube_depth);
        }
        translate([x, y+hole_shift, z -light_tube_depth - Thick*2]) {
            cylinder(d=light_tube_hole_dia, h=light_tube_depth);
        }
        translate([x, y-hole_shift, z -light_tube_depth - Thick*2]) {
            cylinder(d=light_tube_hole_dia, h=light_tube_depth);
        }
    }
}

module light_tube(x, y, z) {
    // Draw the actual light tube
    color("brown", 0.2) {
        translate([x, y, z -light_tube_depth]) {
            difference() {
                cylinder(d=light_tube_od, h=light_tube_height);
                translate([0, 0, -0.1]) {
                    cylinder(d=light_tube_id, h=light_tube_height + 0.2 + 10);
                }
            }
        }
    }
}



module test() {
    difference() {
        color("Orange") {
            cube([_top_x_len, _top_y_len, _top_thick]);
        }

        light_tube_hole(_light_tube_pos_x, _light_tube_pos_y, 0);
    }

    light_tube_holder(_light_tube_pos_x, _light_tube_pos_y, 0);
    %light_tube(_light_tube_pos_x, _light_tube_pos_y, 0);
}


test();
