//
// Dual licensed
// SPDX-License-Identifier: CC-BY-NC-3.0
// SPDX-License-Identifier: CC-BY-SA-4.0
// SPDX-License-Identifier: GPL-3.0-or-later
//


screw_db = [
    // name, dia
    [ "M2", 2 ],
    [ "M2.5", 2.5 ],
    [ "M3", 3 ],
    ];

function screw_lookup(name) = search([name], screw_db, 1, 0);

function screw_get_dia(name) = screw_db[screw_lookup(name)[0]][1];


/*
 * Position should be at the top of the screw hole
 * z in the positive direction will move into the hole
 */
module screw_hole(name, length) {
    dia = screw_get_dia(name);
    cylinder(d=dia, h=length, center=false, $fn=50);
}


