//
// Dual licensed
// SPDX-License-Identifier: CC-BY-NC-3.0
// SPDX-License-Identifier: CC-BY-SA-4.0
// SPDX-License-Identifier: GPL-3.0-or-later
//


function inch2mm(inch) = inch * 25.4;


// These are only used when testing this standalone
Thick = 2;
_top_x_len = 150; // front-to-back
_top_y_len = 200; // side-to-side
_top_thick = 2;
// Center of slot
_light_panel_pos_x = _top_x_len / 3;
_light_panel_pos_y = _top_y_len / 2;


slot_y_len = 135;
slot_x_len = 5.5; // 5 ended up at 4.12 gap when measured
light_panel_slot_depth = 20;
light_panel_height = inch2mm(12);

light_shelf_x_len = 20; // width of star LED with base
light_shelf_y_len = slot_y_len + 0.1;

// 1/8 inch for bracket/heatsink
// about 6-7mm for LEDs plus thermal tape
light_shelf_height = inch2mm(1/8) + 7;

light_base_x_len = light_shelf_x_len + Thick * 2;
light_base_y_len = light_shelf_y_len + Thick * 2;
light_base_depth = light_panel_slot_depth + light_shelf_height + Thick;

zip_hole_dia = 4; // opening in bracket side for zip-ties
led_bracket_thickness = inch2mm(1/8);

// position is center of slot at underside to cover
module light_panel_hole(x, y, z) {
    // Move to center of slot
    translate([x, y, z]) {
        // Carve out space for slot
        translate([0, 0, -light_panel_slot_depth/2 + 0.1]) {
            cube([slot_x_len,
                  slot_y_len,
                  light_panel_slot_depth + Thick*2 + 0.2],
                 center=true);
        }
    }
}


// position is center of slot at underside to cover
module light_panel_holder(x, y, z) {
    difference() {

        // Move to center of slot
        translate([x, y, z]) {
            color("Red") {
                translate([0, 0, -light_base_depth/2]) {
                    cube([light_base_x_len,
                          light_base_y_len,
                          light_base_depth], center=true);
                }
            }
        }

        // Move to center of slot
        translate([x, y, z]) {
            // Carve out space for slot
            translate([0, 0, -light_panel_slot_depth/2 + 0.1]) {
                cube([slot_x_len,
                      slot_y_len,
                      light_panel_slot_depth + Thick*2],
                     center=true);
            }

            // remove space for led shelf
            translate([-Thick, 0,
                       -light_base_depth + Thick+(light_shelf_height)/2]) {
                cube([light_shelf_x_len + Thick*2,
                      light_shelf_y_len,
                      light_shelf_height],
                     center=true);
            }

            // make holes to aid securing LEDs bracket with zip ties
            // how far above bracket shelf to put holes for zip ties
            zip_hole_z_pos = zip_hole_dia/2 + led_bracket_thickness;
            translate([0, 0,
                        -light_base_depth + Thick + zip_hole_z_pos]) {
                rotate([0, 90, 0]) {
                    // light_shelf_y_len
                    inc = light_shelf_y_len / 6;
                    pos = light_shelf_y_len / 2 - inc/2;
                    for (zip_y = [-pos : inc : pos]) {
                        translate([0, zip_y, 0]) {
                            // Make holes hexagon shaped and rotate so flat
                            // edge on on top/bottom when printed
                            rotate([0, 0, 30]) {
                                cylinder(d=zip_hole_dia, h=30, $fn=6);
                            }
                        }
                    }
                }
            }
        }
    }
}


// position is center of slot at underside to cover
module light_panel(x, y, z) {
    color("brown", 0.2) {
        translate([x, y,
                   z -light_panel_slot_depth + light_panel_height/2]) {
            cube([slot_x_len, slot_y_len, light_panel_height], center=true);
        }
    }
}

module test() {
    difference() {
        color("Orange") {
            cube([_top_x_len, _top_y_len, _top_thick]);
        }

        light_panel_hole(_light_panel_pos_x, _light_panel_pos_y, 0);
    }

    light_panel_holder(_light_panel_pos_x, _light_panel_pos_y, 0);
    %light_panel(_light_panel_pos_x, _light_panel_pos_y, 0);
}


module test2() {
    difference() {
        test();
        // Remove '*' to show a cross section
        translate([-1, -1, -50]) {
            cube([250, 50, 100]);
        }
    }
}

test();
