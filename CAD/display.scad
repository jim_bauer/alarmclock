//
// Dual licensed
// SPDX-License-Identifier: CC-BY-NC-3.0
// SPDX-License-Identifier: CC-BY-SA-4.0
// SPDX-License-Identifier: GPL-3.0-or-later
//


//
// Sparkfun Qwiic Alphanumeric Display
// https://www.sparkfun.com/products/16427
//


// Top view
// +----------------------------------------------+
// | o                                          o |
// | +------------------------------------------+ |
// | |                                          | |
// | |               display                    | |
// | |/-disp_pos                                | |
// | +------------------------------------------+ |
// | o                board                     o |
// +----------------------------------------------+
// ^origin                                  hole^
//
// Side view
//   +------------------+
//   |                  |
//   |   display        |
// +v--------------------v+  v=hole
// +----------------------+ board
//


function inch2mm(inch) = inch * 25.4;

// How deep the display portion is
function display_disp_depth(kind) = displays[kind][DISPLAY_DISP][2];

// Translate from Lower-Left corner of display surface to origin of board
function display_digits_to_origin(kind) = [
    - displays[kind][DISPLAY_DISP_POS][0],
    - displays[kind][DISPLAY_DISP_POS][1],
    - (displays[kind][DISPLAY_DISP_POS][2] + displays[kind][DISPLAY_DISP][2])
    ];

function display_disp_width(kind) = displays[kind][DISPLAY_DISP][0];
function display_disp_height(kind) = displays[kind][DISPLAY_DISP][1];



// SparkX Qwiic Alphanumeric display
// https://forum.sparkfun.com/viewtopic.php?t=54496&p=221377#p221377
//   The board is 1.6" x 1.1". Holes are on 1.4" and 0.9" spacing, 0.13" in
//   diameter.  The display is 1.575" x 0.625"
// All values from CAD drawing except...
//   disp[0]: from above URL
//   disp[1]: measured
//   disp[2]: measured
//   disp_pos: assumed centered x and y
qwiic_alpha_board = [ 40.64, 27.94, 1.61];
qwiic_alpha_disp = [ inch2mm(1.575), 16.03, 9.75 - qwiic_alpha_board[2]];
qwiic_alpha_disp_pos = [(qwiic_alpha_board[0] - qwiic_alpha_disp[0])/2,
                        (qwiic_alpha_board[1] - qwiic_alpha_disp[1])/2,
                        qwiic_alpha_board[2]];
qwiic_alpha_dia = 3.3;
qwiic_alpha_holes = [
    [2.54, 2.54],
    [38.1, 3.1],
    [38.1, 25.4],
    [2.54, 25.4],
    ];


// Adafruit 1.2" 7-segment LED
ada127_board = [inch2mm(4.71), inch2mm(2), 1.65]; // thickness measured - recheck
ada127_disp = [inch2mm(4.71), inch2mm(1.6), 11.19 - ada127_board[2]]; // fix thickness
ada127_disp_pos = [0, inch2mm(2-1.6)/2, ada127_board[2]];
ada127_dia = 1.9; // measured
ada127_holes = [
    [ inch2mm(0.14), inch2mm(0.1)],
    [ inch2mm(4.71 - 0.14), inch2mm(0.1)],
    [ inch2mm(4.71 - 0.14), inch2mm(2 - 0.1)],
    [ inch2mm(0.14), inch2mm(2 - 0.1)],
    ];


display_font = "Liberation Mono";
display_fontI = "Liberation Mono: style=Italic";

// Indexes into displays
DISPLAY_QWIIC_ALPHA = 0;
DISPLAY_ADAFRUIT_12 = 1;

// Indexes into display array
DISPLAY_TEXT1 = 0;
DISPLAY_TEXT2 = 1;
DISPLAY_TEXT3 = 2;
DISPLAY_BOARD = 3;
DISPLAY_DISP = 4;
DISPLAY_DISP_POS = 5;
DISPLAY_DIA = 6;
DISPLAY_HOLES = 7;
DISPLAY_COLORS = 8;

displays = [
    [
        [ "SPARX Qwiic Alphanumeric Display", display_font, 1, "white"],
        [ "ALPH", display_fontI, 10, "red"],
        [ "", display_font, 1, "white"],
        qwiic_alpha_board,
        qwiic_alpha_disp,
        qwiic_alpha_disp_pos,
        qwiic_alpha_dia,
        qwiic_alpha_holes,
        [ "black", "white", "black" ], // board, disp sides, disp top
        ],
    [
        ["IO + - D C", display_font, 1, "white"],
        ["88:88", display_fontI, 29, "red"],
        ["1.2\" 7-segment LED HT16K33 Backpack", display_font, 2, "white"],
        ada127_board,
        ada127_disp,
        ada127_disp_pos,
        ada127_dia,
        ada127_holes,
        [ "navy", "white", "black" ],
        ],
    ];


// Create a partial frame around the display
// Just the top and bottom, but not all the way across as
// at least one board type has solder pins near the center
module display_frame(kind, zxtra=0, screw_name="M3", screw_len=5,
                     color="blue") {
    display = displays[kind];

    board = display[DISPLAY_BOARD];
    disp = display[DISPLAY_DISP];
    disp_pos = display[DISPLAY_DISP_POS];
    dia = display[DISPLAY_DIA];
    holes = display[DISPLAY_HOLES];

    translate([0, 0, board[2]]) { // move to top surface of board
        difference() {
            color(color) {
                // Bottom left
                translate([0, 0, 0]) {
                    cube([board[0] / 4, disp_pos[1], disp[2] + zxtra]);
                }
                // Bottom right
                translate([board[0] * 3/4, 0, 0]) {
                    cube([board[0] / 4, disp_pos[1], disp[2] + zxtra]);
                }
                // top right
                translate([board[0] * 3/4, disp_pos[1] + disp[1], 0]) {
                    cube([board[0] / 4, disp_pos[1], disp[2] + zxtra]);
                }
                // top left
                translate([0, disp_pos[1] + disp[1], 0]) {
                    cube([board[0] / 4, disp_pos[1], disp[2] + zxtra]);
                }
            }

            // Remove the screw holes
            for (hole = holes) {
                translate([hole[0], hole[1], -0.1]) {
                    screw_hole(screw_name, screw_len + 0.1);
                }
            }
        }
    }
}


module display(kind) {
    display = displays[kind];

    text1 = display[DISPLAY_TEXT1];
    text2 = display[DISPLAY_TEXT2];
    text3 = display[DISPLAY_TEXT3];
    board = display[DISPLAY_BOARD];
    disp = display[DISPLAY_DISP];
    disp_pos = display[DISPLAY_DISP_POS];
    dia = display[DISPLAY_DIA];
    holes = display[DISPLAY_HOLES];
    colors = display[DISPLAY_COLORS]; // 0: board, 1: disp side, 2: disp top
    
    difference() {
        union() {
            // Board
            color(colors[0])
                cube(board);

            // Text on top edge of board
            translate([board[0]/2, board[1]-1, board[2]]) {
                color(text1[3]) {
                    %text(text1[0], font=text1[1], size=text1[2],
                         valign="top", halign="center");
                }
            }

            // Text on bottom edge of board
            translate([board[0]/2, 1, board[2]]) {
                color(text3[3]) {
                    %text(text3[0], font=text3[1], size=text3[2],
                         valign="bottom", halign="center");
                }
            }
        
            // Display
            translate(disp_pos) {
                // Main bulk of display
                color(colors[1]) {
                    cube(disp);
                }

                // Color top surface of display
                color(colors[2]) {
                    translate([0, 0, disp[2] - 0.01]) {
                        cube([disp[0], disp[1], 0.02]);
                    }
                }

                // Text (digits) on display
                color(text2[3]) {
                    translate([disp[0]/2, disp[1]/2, disp[2]])
                        %text(text2[0], font=text2[1], size=text2[2],
                             valign="center", halign="center");
                }
            }
        }

        // remove the holes
        for (hole = holes) {
            translate(hole) {
                // Move up to 1/2 board thickness and put cylinder
                // there (+1 unit to remove artifacts)
                translate([0, 0, board[2]/2]) {
                    color(colors[0])
                        cylinder(d = dia, h=board[2]+1, center=true, $fn=100);
                }
            }
        }
    }
}

//display(DISPLAY_QWIIC_ALPHA);
//translate([50, 0, 0])
//display(DISPLAY_ADAFRUIT_12);
