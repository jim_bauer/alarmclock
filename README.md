# Alarm clock with wakeup light

This is an alarm clock / wakeup light project controlled via an
Arduino with a 3D printered enclosure


![clockon](images/clock-light-on.jpg)


![clockoff](images/clock-light-off.jpg)


[Wakeup time-lapse](images/wakeup.gif)



## Features

### Desired features

When this project started, these were the desired targeted features

- No snooze functionality (evil)
- Accurate
- Auto Daylight Saving Time adjust
- Alarm still sounds even if power is out (for at least one day)
- Display current time and alarm time at the same time
- Display times in 24-hour time (legacy 12-hour is too mistake prone)
- Alarm should not be anoying
- Ability to control separate wake-up light (or have it builtin)
- One action to silence alarm and be ready for next day.
- Easy to read current time even when half asleep


### Actual features

- No snoose.  (That one was easy.)
- Accurate  
  The RTC used has its own internal battery and is accurate to within ±1.5 ppm.  That's 0.13 seconds per day, or 0.79 minutes per year.
- Auto Daylight Saving Time Adjust
- Alarm still sounds even if power is out  
  The 12V 3000mAh external battery pack will keep the alarm clock
  functioning (without wakeup light) for a long time.  XXX calculate
  how long
- Displays current time and well as alarm time.  
  The addition of a second smaller display shows the currently set
  alarm time (or "OFF").  When setting the time/alarm, the small display
  shows what is currently being set.
- Displays times in 24-hour time
- Alarm is not anoying  
  This is certainly subjective, but I believe this target has been
  met.  I modeled the beginning part of the alarm sequence to a small
  battery operated travel alarm clock I liked.  It will get a bit more
  persistent over time, but nothing too anoying (I hope).  For those
  with hearing issue, it may not be loud enough for you.
- Has builtin wake-up light  
  The light comes on 30 minutes before the alarm sounds and ramps up
  slowly to full intensity by the alarm time.  It will then stay at
  full for 15 minutes, then ramp back down to off over another 5
  minutes.
- One action to silence alarm and be ready for next day  
  With the capacitive touch on the top-front-center of the enclosure,
  you just have to barely touch the top of the enclosure to silence
  the alarm.  And of course, nothing extra is needed for it to be
  ready the sound the next day.
- Easy to read current time even when half asleep  
  Large 1.2" 4-digit display is easy to read
- Maintains time when power is off  
  Even without the external battery pack, the clock will still keep time
  as the builtin RTC has its one battery.
- Sleep mode  
  The capacitive touch can be used to activate a sleep mode, where the light comes on at about half strength, and then ramps down to off over a 2 minute period.  Plenty of time to turn off other lights and get into bed.
- Can silence alarm before it sounds  
  After the wakeup light starts to come on, but before the alarm even
  sounds, you can touch the capacitive touch and the alarm will be
  prevented for sounding at all.
- 3d printable custom enclosure


## Creating this yourself

If you wish to build something like this, check out the [Build and Assembly instructions](doc/assembly.md)


## Enhancements

A custom PCB would simplify electronics, but add to the cost.


## Licensing

Most of this project is licensed under the GNU GPL (GPL-3.0-or-later).  See the [LICENSE](LICENSE) file in this directory.
However the contents of the CAD/ directory are [seperately licensed](CAD/LICENSE) under the [Creative Commons - Attribution -
Non-Commercial (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/) license.

